import { Component, OnInit } from "@angular/core";
import {Router} from "@angular/router";

/* ***********************************************************
* Before you can navigate to this page from your app, you need to reference this page's module in the
* global app router module. Add the following object to the global array of routes:
* { path: "Caja", loadChildren: "./Caja/Caja.module#CajaModule" }
* Note that this simply points the path to the page module file. If you move the page, you need to update the route too.
*************************************************************/

@Component({
    selector: "Clientes",
    moduleId: module.id,
    templateUrl: "./Clientes.component.html",
    animations: [
        "safe"
      ]
})
export class ClientesComponent implements OnInit {
    email: string;
    password: string;

    constructor(private router: Router) {
        /* ***********************************************************
        * Use the constructor to inject app services that you need in this component.
        *************************************************************/
    }

    ngOnInit(): void {
        /* ***********************************************************
        * Use the "ngOnInit" handler to initialize data for this component.
        *************************************************************/
    }
}
