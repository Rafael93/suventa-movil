"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var nativescript_angular_1 = require("nativescript-angular");
var RGBComponent = /** @class */ (function () {
    function RGBComponent(router, route) {
        this.router = router;
        this.route = route;
        this.rgb = 'black';
    }
    RGBComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params
            .forEach(function (params) { return _this.rgb = params['rgb']; });
    };
    RGBComponent.prototype.goBlue = function () {
        this.router.navigate(['/color/blue']);
    };
    RGBComponent.prototype.goRed = function () {
        this.router.navigate(['/color/red']);
    };
    RGBComponent.prototype.changeToRandom = function () {
        this.router.navigate(['/color/rgb', this.getRandomColor()]);
    };
    RGBComponent.prototype.getRandomColor = function () {
        var r = Math.floor(Math.random() * 16).toString(16);
        var g = Math.floor(Math.random() * 16).toString(16);
        var b = Math.floor(Math.random() * 16).toString(16);
        return '#' + r + g + b;
    };
    RGBComponent.prototype.goBack = function () {
        this.router.back();
    };
    RGBComponent.prototype.goHome = function () {
        this.router.navigate(['/color'], { clearHistory: true });
    };
    RGBComponent = __decorate([
        core_1.Component({
            selector: 'my-color-paletter',
            moduleId: module.id,
            templateUrl: './rgb.component.html',
            styleUrls: ['./color.component.css']
        }),
        __metadata("design:paramtypes", [nativescript_angular_1.RouterExtensions, router_1.ActivatedRoute])
    ], RGBComponent);
    return RGBComponent;
}());
exports.RGBComponent = RGBComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmdiLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInJnYi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBa0Q7QUFDbEQsMENBQWlEO0FBQ2pELDZEQUF3RDtBQVF4RDtJQUdFLHNCQUFvQixNQUF3QixFQUFVLEtBQXFCO1FBQXZELFdBQU0sR0FBTixNQUFNLENBQWtCO1FBQVUsVUFBSyxHQUFMLEtBQUssQ0FBZ0I7UUFGM0UsUUFBRyxHQUFXLE9BQU8sQ0FBQztJQUd0QixDQUFDO0lBRUQsK0JBQVEsR0FBUjtRQUFBLGlCQUdDO1FBRkMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNO2FBQ2hCLE9BQU8sQ0FBQyxVQUFBLE1BQU0sSUFBSSxPQUFBLEtBQUksQ0FBQyxHQUFHLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUF4QixDQUF3QixDQUFDLENBQUM7SUFDL0MsQ0FBQztJQUVELDZCQUFNLEdBQU47UUFDRSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7SUFDeEMsQ0FBQztJQUVELDRCQUFLLEdBQUw7UUFDRSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVELHFDQUFjLEdBQWQ7UUFDRSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRCxxQ0FBYyxHQUFkO1FBQ0UsSUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3RELElBQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUN0RCxJQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDdEQsT0FBTyxHQUFHLEdBQUcsQ0FBQyxHQUFDLENBQUMsR0FBQyxDQUFDLENBQUM7SUFDckIsQ0FBQztJQUVELDZCQUFNLEdBQU47UUFDRSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3JCLENBQUM7SUFFRCw2QkFBTSxHQUFOO1FBQ0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQzNELENBQUM7SUFwQ1UsWUFBWTtRQU54QixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLG1CQUFtQjtZQUM3QixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLHNCQUFzQjtZQUNuQyxTQUFTLEVBQUUsQ0FBQyx1QkFBdUIsQ0FBQztTQUNyQyxDQUFDO3lDQUk0Qix1Q0FBZ0IsRUFBaUIsdUJBQWM7T0FIaEUsWUFBWSxDQXNDeEI7SUFBRCxtQkFBQztDQUFBLEFBdENELElBc0NDO0FBdENZLG9DQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSAnbmF0aXZlc2NyaXB0LWFuZ3VsYXInO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdteS1jb2xvci1wYWxldHRlcicsXHJcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICB0ZW1wbGF0ZVVybDogJy4vcmdiLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9jb2xvci5jb21wb25lbnQuY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIFJHQkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdHtcclxuICByZ2I6IHN0cmluZyA9ICdibGFjayc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyOiBSb3V0ZXJFeHRlbnNpb25zLCBwcml2YXRlIHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSkge1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLnJvdXRlLnBhcmFtc1xyXG4gICAgLmZvckVhY2gocGFyYW1zID0+IHRoaXMucmdiID0gcGFyYW1zWydyZ2InXSk7XHJcbiAgfVxyXG5cclxuICBnb0JsdWUoKSB7XHJcbiAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy9jb2xvci9ibHVlJ10pO1xyXG4gIH1cclxuXHJcbiAgZ29SZWQoKSB7XHJcbiAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy9jb2xvci9yZWQnXSk7XHJcbiAgfVxyXG5cclxuICBjaGFuZ2VUb1JhbmRvbSgpIHtcclxuICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL2NvbG9yL3JnYicsIHRoaXMuZ2V0UmFuZG9tQ29sb3IoKV0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0UmFuZG9tQ29sb3IoKTogc3RyaW5nIHtcclxuICAgIGNvbnN0IHIgPSBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiAxNikudG9TdHJpbmcoMTYpO1xyXG4gICAgY29uc3QgZyA9IE1hdGguZmxvb3IoTWF0aC5yYW5kb20oKSAqIDE2KS50b1N0cmluZygxNik7XHJcbiAgICBjb25zdCBiID0gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogMTYpLnRvU3RyaW5nKDE2KTtcclxuICAgIHJldHVybiAnIycgKyByK2crYjtcclxuICB9XHJcblxyXG4gIGdvQmFjaygpIHtcclxuICAgIHRoaXMucm91dGVyLmJhY2soKTtcclxuICB9XHJcblxyXG4gIGdvSG9tZSgpIHtcclxuICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL2NvbG9yJ10sIHsgY2xlYXJIaXN0b3J5OiB0cnVlIH0pO1xyXG4gIH1cclxuICBcclxufVxyXG4iXX0=