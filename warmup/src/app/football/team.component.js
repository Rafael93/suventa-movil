"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var football_service_1 = require("../football.service");
var TeamComponent = /** @class */ (function () {
    // private players: Player[] = [];
    function TeamComponent(route, footballService) {
        this.route = route;
        this.footballService = footballService;
        this.fixtures = [];
    }
    TeamComponent.prototype.ngOnInit = function () {
        var _this = this;
        var teamId = +this.route.snapshot.params['teamId'];
        this.footballService.getTeam(teamId)
            .subscribe(function (team) { return _this.team = team; });
        this.footballService.getTeamFixtures(teamId)
            .subscribe(function (fixtures) { return _this.fixtures = fixtures; });
        // this.footballService.getPlayers(teamId)
        // .subscribe(players => this.players = players);
    };
    TeamComponent.prototype.teamSelected = function (teamId) {
        console.log('::TeamComponent::teamSelected::' + teamId);
    };
    TeamComponent = __decorate([
        core_1.Component({
            selector: 'my-team',
            moduleId: module.id,
            templateUrl: './team.component.html'
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            football_service_1.FootballService])
    ], TeamComponent);
    return TeamComponent;
}());
exports.TeamComponent = TeamComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVhbS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ0ZWFtLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCwwQ0FBaUQ7QUFFakQsd0RBQXNEO0FBUXREO0lBR0Usa0NBQWtDO0lBRWxDLHVCQUNVLEtBQXFCLEVBQ3JCLGVBQWdDO1FBRGhDLFVBQUssR0FBTCxLQUFLLENBQWdCO1FBQ3JCLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUxsQyxhQUFRLEdBQWMsRUFBRSxDQUFDO0lBTWpDLENBQUM7SUFFRCxnQ0FBUSxHQUFSO1FBQUEsaUJBV0M7UUFWQyxJQUFNLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUVyRCxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUM7YUFDakMsU0FBUyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLElBQUksR0FBRyxJQUFJLEVBQWhCLENBQWdCLENBQUMsQ0FBQztRQUV2QyxJQUFJLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUM7YUFDekMsU0FBUyxDQUFDLFVBQUEsUUFBUSxJQUFJLE9BQUEsS0FBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLEVBQXhCLENBQXdCLENBQUMsQ0FBQTtRQUVsRCwwQ0FBMEM7UUFDMUMsaURBQWlEO0lBQ25ELENBQUM7SUFFRCxvQ0FBWSxHQUFaLFVBQWEsTUFBYztRQUN6QixPQUFPLENBQUMsR0FBRyxDQUFDLGlDQUFpQyxHQUFHLE1BQU0sQ0FBQyxDQUFDO0lBQzFELENBQUM7SUF6QlUsYUFBYTtRQUx6QixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLFNBQVM7WUFDbkIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSx1QkFBdUI7U0FDckMsQ0FBQzt5Q0FPaUIsdUJBQWM7WUFDSixrQ0FBZTtPQVAvQixhQUFhLENBMEJ6QjtJQUFELG9CQUFDO0NBQUEsQUExQkQsSUEwQkM7QUExQlksc0NBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcblxyXG5pbXBvcnQgeyBGb290YmFsbFNlcnZpY2UgfSBmcm9tICcuLi9mb290YmFsbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVGVhbSwgRml4dHVyZSwgUGxheWVyIH0gZnJvbSAnLi4vbW9kZWxzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbXktdGVhbScsXHJcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICB0ZW1wbGF0ZVVybDogJy4vdGVhbS5jb21wb25lbnQuaHRtbCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFRlYW1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXR7XHJcbiAgcHJpdmF0ZSB0ZWFtOiBUZWFtO1xyXG4gIHByaXZhdGUgZml4dHVyZXM6IEZpeHR1cmVbXSA9IFtdO1xyXG4gIC8vIHByaXZhdGUgcGxheWVyczogUGxheWVyW10gPSBbXTtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcclxuICAgIHByaXZhdGUgZm9vdGJhbGxTZXJ2aWNlOiBGb290YmFsbFNlcnZpY2UpIHtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgY29uc3QgdGVhbUlkID0gK3RoaXMucm91dGUuc25hcHNob3QucGFyYW1zWyd0ZWFtSWQnXTtcclxuXHJcbiAgICB0aGlzLmZvb3RiYWxsU2VydmljZS5nZXRUZWFtKHRlYW1JZClcclxuICAgICAgLnN1YnNjcmliZSh0ZWFtID0+IHRoaXMudGVhbSA9IHRlYW0pO1xyXG5cclxuICAgIHRoaXMuZm9vdGJhbGxTZXJ2aWNlLmdldFRlYW1GaXh0dXJlcyh0ZWFtSWQpXHJcbiAgICAgIC5zdWJzY3JpYmUoZml4dHVyZXMgPT4gdGhpcy5maXh0dXJlcyA9IGZpeHR1cmVzKSBcclxuXHJcbiAgICAvLyB0aGlzLmZvb3RiYWxsU2VydmljZS5nZXRQbGF5ZXJzKHRlYW1JZClcclxuICAgIC8vIC5zdWJzY3JpYmUocGxheWVycyA9PiB0aGlzLnBsYXllcnMgPSBwbGF5ZXJzKTtcclxuICB9XHJcblxyXG4gIHRlYW1TZWxlY3RlZCh0ZWFtSWQ6IG51bWJlcikge1xyXG4gICAgY29uc29sZS5sb2coJzo6VGVhbUNvbXBvbmVudDo6dGVhbVNlbGVjdGVkOjonICsgdGVhbUlkKTtcclxuICB9XHJcbn1cclxuIl19