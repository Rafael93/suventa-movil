"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var TablesComponent = /** @class */ (function () {
    function TablesComponent(router) {
        this.router = router;
        this.PremierLeagueId = 445;
        this.PrimeraDivisionId = 455;
        this.BundesligaId = 452;
        this.SerieAId = 456;
        this.Ligue1Id = 450;
        this.EredivisieId = 449;
    }
    TablesComponent.prototype.onTeamTap = function (teamId) {
        console.log('::TablesComponent::onTeamTap::' + teamId);
        this.router.navigate(['/football/team', teamId]);
    };
    TablesComponent = __decorate([
        core_1.Component({
            selector: 'my-tables',
            moduleId: module.id,
            templateUrl: './tables.component.html'
        }),
        __metadata("design:paramtypes", [router_1.Router])
    ], TablesComponent);
    return TablesComponent;
}());
exports.TablesComponent = TablesComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGVzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInRhYmxlcy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBa0Q7QUFDbEQsMENBQXlDO0FBU3pDO0lBU0UseUJBQW9CLE1BQWM7UUFBZCxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBUDNCLG9CQUFlLEdBQVcsR0FBRyxDQUFDO1FBQzlCLHNCQUFpQixHQUFXLEdBQUcsQ0FBQztRQUNoQyxpQkFBWSxHQUFXLEdBQUcsQ0FBQztRQUMzQixhQUFRLEdBQVcsR0FBRyxDQUFDO1FBQ3ZCLGFBQVEsR0FBVyxHQUFHLENBQUM7UUFDdkIsaUJBQVksR0FBVyxHQUFHLENBQUM7SUFHbEMsQ0FBQztJQUVPLG1DQUFTLEdBQWpCLFVBQWtCLE1BQWM7UUFDOUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQ0FBZ0MsR0FBRyxNQUFNLENBQUMsQ0FBQztRQUN2RCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGdCQUFnQixFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUM7SUFDbkQsQ0FBQztJQWZVLGVBQWU7UUFMM0IsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxXQUFXO1lBQ3JCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixXQUFXLEVBQUUseUJBQXlCO1NBQ3ZDLENBQUM7eUNBVTRCLGVBQU07T0FUdkIsZUFBZSxDQWdCM0I7SUFBRCxzQkFBQztDQUFBLEFBaEJELElBZ0JDO0FBaEJZLDBDQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuXHJcbmltcG9ydCB7IENvbXBldGl0aW9uLCBMZWFndWVUYWJsZSB9IGZyb20gJy4uL21vZGVscyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ215LXRhYmxlcycsXHJcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICB0ZW1wbGF0ZVVybDogJy4vdGFibGVzLmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgVGFibGVzQ29tcG9uZW50IHtcclxuXHJcbiAgcHVibGljIFByZW1pZXJMZWFndWVJZDogbnVtYmVyID0gNDQ1O1xyXG4gIHB1YmxpYyBQcmltZXJhRGl2aXNpb25JZDogbnVtYmVyID0gNDU1O1xyXG4gIHB1YmxpYyBCdW5kZXNsaWdhSWQ6IG51bWJlciA9IDQ1MjtcclxuICBwdWJsaWMgU2VyaWVBSWQ6IG51bWJlciA9IDQ1NjtcclxuICBwdWJsaWMgTGlndWUxSWQ6IG51bWJlciA9IDQ1MDtcclxuICBwdWJsaWMgRXJlZGl2aXNpZUlkOiBudW1iZXIgPSA0NDk7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyOiBSb3V0ZXIpIHtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgb25UZWFtVGFwKHRlYW1JZDogbnVtYmVyKSB7XHJcbiAgICBjb25zb2xlLmxvZygnOjpUYWJsZXNDb21wb25lbnQ6Om9uVGVhbVRhcDo6JyArIHRlYW1JZCk7XHJcbiAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy9mb290YmFsbC90ZWFtJywgdGVhbUlkXSk7XHJcbiAgfVxyXG59XHJcbiJdfQ==