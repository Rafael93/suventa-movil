"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var football_service_1 = require("../football.service");
var CompetitionFixturesComponent = /** @class */ (function () {
    function CompetitionFixturesComponent(footballService, route) {
        this.footballService = footballService;
        this.route = route;
        this.fixtures = [];
        this.competitionName = '';
    }
    CompetitionFixturesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.competitionId = +this.route.snapshot.params['competitionId'];
        this.competitionName = this.route.snapshot.params['competitionName'];
        this.footballService.getFixtures(this.competitionId)
            .subscribe(function (fixtures) { return _this.fixtures = fixtures; });
    };
    CompetitionFixturesComponent = __decorate([
        core_1.Component({
            selector: 'my-fixtures',
            moduleId: module.id,
            templateUrl: './competition-fixtures.component.html',
            styleUrls: ['./fixture.component.css']
        }),
        __metadata("design:paramtypes", [football_service_1.FootballService,
            router_1.ActivatedRoute])
    ], CompetitionFixturesComponent);
    return CompetitionFixturesComponent;
}());
exports.CompetitionFixturesComponent = CompetitionFixturesComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcGV0aXRpb24tZml4dHVyZXMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcGV0aXRpb24tZml4dHVyZXMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBRWxELDBDQUFpRDtBQUdqRCx3REFBc0Q7QUFRdEQ7SUFLRSxzQ0FDVSxlQUFnQyxFQUNoQyxLQUFxQjtRQURyQixvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFDaEMsVUFBSyxHQUFMLEtBQUssQ0FBZ0I7UUFOeEIsYUFBUSxHQUFjLEVBQUUsQ0FBQztRQUV6QixvQkFBZSxHQUFXLEVBQUUsQ0FBQztJQUtwQyxDQUFDO0lBRUQsK0NBQVEsR0FBUjtRQUFBLGlCQU1DO1FBTEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUNsRSxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBRXJFLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUM7YUFDakQsU0FBUyxDQUFDLFVBQUEsUUFBUSxJQUFJLE9BQUEsS0FBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLEVBQXhCLENBQXdCLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBaEJVLDRCQUE0QjtRQU54QyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGFBQWE7WUFDdkIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSx1Q0FBdUM7WUFDcEQsU0FBUyxFQUFFLENBQUMseUJBQXlCLENBQUM7U0FDdkMsQ0FBQzt5Q0FPMkIsa0NBQWU7WUFDekIsdUJBQWM7T0FQcEIsNEJBQTRCLENBaUJ4QztJQUFELG1DQUFDO0NBQUEsQUFqQkQsSUFpQkM7QUFqQlksb0VBQTRCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5cclxuaW1wb3J0IHsgRml4dHVyZSwgVGVhbSB9IGZyb20gJy4uL21vZGVscyc7XHJcbmltcG9ydCB7IEZvb3RiYWxsU2VydmljZSB9IGZyb20gJy4uL2Zvb3RiYWxsLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdteS1maXh0dXJlcycsXHJcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICB0ZW1wbGF0ZVVybDogJy4vY29tcGV0aXRpb24tZml4dHVyZXMuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2ZpeHR1cmUuY29tcG9uZW50LmNzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb21wZXRpdGlvbkZpeHR1cmVzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0e1xyXG4gIHB1YmxpYyBmaXh0dXJlczogRml4dHVyZVtdID0gW107XHJcbiAgcHVibGljIGNvbXBldGl0aW9uSWQ6IG51bWJlcjtcclxuICBwdWJsaWMgY29tcGV0aXRpb25OYW1lOiBzdHJpbmcgPSAnJztcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIGZvb3RiYWxsU2VydmljZTogRm9vdGJhbGxTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSByb3V0ZTogQWN0aXZhdGVkUm91dGUpIHtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5jb21wZXRpdGlvbklkID0gK3RoaXMucm91dGUuc25hcHNob3QucGFyYW1zWydjb21wZXRpdGlvbklkJ107XHJcbiAgICB0aGlzLmNvbXBldGl0aW9uTmFtZSA9IHRoaXMucm91dGUuc25hcHNob3QucGFyYW1zWydjb21wZXRpdGlvbk5hbWUnXTtcclxuXHJcbiAgICB0aGlzLmZvb3RiYWxsU2VydmljZS5nZXRGaXh0dXJlcyh0aGlzLmNvbXBldGl0aW9uSWQpXHJcbiAgICAgIC5zdWJzY3JpYmUoZml4dHVyZXMgPT4gdGhpcy5maXh0dXJlcyA9IGZpeHR1cmVzKTtcclxuICB9XHJcbn1cclxuIl19