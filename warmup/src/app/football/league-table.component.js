"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var football_service_1 = require("../football.service");
var LeagueTableComponent = /** @class */ (function () {
    function LeagueTableComponent(footballService) {
        this.footballService = footballService;
    }
    LeagueTableComponent.prototype.ngOnInit = function () {
        this.loadTeamsAndTable();
    };
    /**
     * Get both teams and table info. Teams contains short name for each team
     */
    LeagueTableComponent.prototype.loadTeamsAndTable = function () {
        var _this = this;
        this.footballService.getTeams(this.competitionId)
            .subscribe(function (teams) {
            _this.teams = teams;
            _this.footballService.getLeagueTable(_this.competitionId)
                .subscribe(function (table) { return _this.table = table; });
        });
    };
    LeagueTableComponent.prototype.getTeamName = function (teamId) {
        var team = this.getTeam(teamId);
        return (team.shortName) ? team.shortName : team.name;
    };
    LeagueTableComponent.prototype.getTeam = function (teamId) {
        return this.teams.filter(function (team) { return team.teamId === teamId; })[0];
    };
    LeagueTableComponent.prototype.onTeamSelected = function (event) {
        var selectedTeamId = this.table.standing[event.index].teamId;
        console.log('::LeagueTableComponent::onTeamSelect::' + selectedTeamId);
        // Add eventemitter emit call here with the selectedTeamId
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], LeagueTableComponent.prototype, "competitionId", void 0);
    LeagueTableComponent = __decorate([
        core_1.Component({
            selector: 'my-league-table',
            moduleId: module.id,
            templateUrl: './league-table.component.html'
        }),
        __metadata("design:paramtypes", [football_service_1.FootballService])
    ], LeagueTableComponent);
    return LeagueTableComponent;
}());
exports.LeagueTableComponent = LeagueTableComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGVhZ3VlLXRhYmxlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImxlYWd1ZS10YWJsZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBK0U7QUFJL0Usd0RBQXNEO0FBT3REO0lBT0UsOEJBQW9CLGVBQWdDO1FBQWhDLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtJQUNwRCxDQUFDO0lBRUQsdUNBQVEsR0FBUjtRQUNFLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO0lBQzNCLENBQUM7SUFFRDs7T0FFRztJQUNLLGdEQUFpQixHQUF6QjtRQUFBLGlCQU9DO1FBTkMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQzthQUM5QyxTQUFTLENBQUMsVUFBQSxLQUFLO1lBQ2QsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7WUFDbkIsS0FBSSxDQUFDLGVBQWUsQ0FBQyxjQUFjLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQztpQkFDcEQsU0FBUyxDQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFLLEVBQWxCLENBQWtCLENBQUMsQ0FBQztRQUM1QyxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTSwwQ0FBVyxHQUFsQixVQUFtQixNQUFjO1FBQy9CLElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFbEMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztJQUN2RCxDQUFDO0lBRU8sc0NBQU8sR0FBZixVQUFnQixNQUFjO1FBQzVCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsTUFBTSxLQUFLLE1BQU0sRUFBdEIsQ0FBc0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRCw2Q0FBYyxHQUFkLFVBQWUsS0FBSztRQUNsQixJQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxDQUFDO1FBQy9ELE9BQU8sQ0FBQyxHQUFHLENBQUMsd0NBQXdDLEdBQUcsY0FBYyxDQUFDLENBQUM7UUFDdkUsMERBQTBEO0lBQzVELENBQUM7SUF2Q1E7UUFBUixZQUFLLEVBQUU7OytEQUE4QjtJQUQzQixvQkFBb0I7UUFMaEMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxpQkFBaUI7WUFDM0IsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSwrQkFBK0I7U0FDN0MsQ0FBQzt5Q0FRcUMsa0NBQWU7T0FQekMsb0JBQW9CLENBeUNoQztJQUFELDJCQUFDO0NBQUEsQUF6Q0QsSUF5Q0M7QUF6Q1ksb0RBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG4vLyBpbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcy9PYnNlcnZhYmxlJztcclxuXHJcbmltcG9ydCB7IExlYWd1ZVRhYmxlLCBUZWFtIH0gZnJvbSAnLi4vbW9kZWxzJztcclxuaW1wb3J0IHsgRm9vdGJhbGxTZXJ2aWNlIH0gZnJvbSAnLi4vZm9vdGJhbGwuc2VydmljZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ215LWxlYWd1ZS10YWJsZScsXHJcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICB0ZW1wbGF0ZVVybDogJy4vbGVhZ3VlLXRhYmxlLmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTGVhZ3VlVGFibGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXR7XHJcbiAgQElucHV0KCkgcHVibGljIGNvbXBldGl0aW9uSWQ6IG51bWJlcjtcclxuICAvLyBBZGQgT3V0cHV0IEV2ZW50RW1pdHRlciBoZXJlXHJcblxyXG4gIHB1YmxpYyB0YWJsZTogTGVhZ3VlVGFibGU7XHJcbiAgcHVibGljIHRlYW1zOiBUZWFtW107XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZm9vdGJhbGxTZXJ2aWNlOiBGb290YmFsbFNlcnZpY2UpIHtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5sb2FkVGVhbXNBbmRUYWJsZSgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogR2V0IGJvdGggdGVhbXMgYW5kIHRhYmxlIGluZm8uIFRlYW1zIGNvbnRhaW5zIHNob3J0IG5hbWUgZm9yIGVhY2ggdGVhbVxyXG4gICAqL1xyXG4gIHByaXZhdGUgbG9hZFRlYW1zQW5kVGFibGUoKSB7XHJcbiAgICB0aGlzLmZvb3RiYWxsU2VydmljZS5nZXRUZWFtcyh0aGlzLmNvbXBldGl0aW9uSWQpXHJcbiAgICAgIC5zdWJzY3JpYmUodGVhbXMgPT4ge1xyXG4gICAgICAgIHRoaXMudGVhbXMgPSB0ZWFtcztcclxuICAgICAgICB0aGlzLmZvb3RiYWxsU2VydmljZS5nZXRMZWFndWVUYWJsZSh0aGlzLmNvbXBldGl0aW9uSWQpXHJcbiAgICAgICAgICAuc3Vic2NyaWJlKHRhYmxlID0+IHRoaXMudGFibGUgPSB0YWJsZSk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldFRlYW1OYW1lKHRlYW1JZDogbnVtYmVyKTogc3RyaW5nIHtcclxuICAgIGNvbnN0IHRlYW0gPSB0aGlzLmdldFRlYW0odGVhbUlkKTtcclxuXHJcbiAgICByZXR1cm4gKHRlYW0uc2hvcnROYW1lKSA/IHRlYW0uc2hvcnROYW1lIDogdGVhbS5uYW1lO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBnZXRUZWFtKHRlYW1JZDogbnVtYmVyKTogVGVhbSB7XHJcbiAgICByZXR1cm4gdGhpcy50ZWFtcy5maWx0ZXIodGVhbSA9PiB0ZWFtLnRlYW1JZCA9PT0gdGVhbUlkKVswXTtcclxuICB9XHJcblxyXG4gIG9uVGVhbVNlbGVjdGVkKGV2ZW50KSB7XHJcbiAgICBjb25zdCBzZWxlY3RlZFRlYW1JZCA9IHRoaXMudGFibGUuc3RhbmRpbmdbZXZlbnQuaW5kZXhdLnRlYW1JZDtcclxuICAgIGNvbnNvbGUubG9nKCc6OkxlYWd1ZVRhYmxlQ29tcG9uZW50OjpvblRlYW1TZWxlY3Q6OicgKyBzZWxlY3RlZFRlYW1JZCk7XHJcbiAgICAvLyBBZGQgZXZlbnRlbWl0dGVyIGVtaXQgY2FsbCBoZXJlIHdpdGggdGhlIHNlbGVjdGVkVGVhbUlkXHJcbiAgfVxyXG59XHJcbiJdfQ==