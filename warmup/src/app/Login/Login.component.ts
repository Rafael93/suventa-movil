import { Component, OnInit } from "@angular/core";
import {Router} from "@angular/router";
import * as dialogs from "tns-core-modules/ui/dialogs";

/* ***********************************************************
* Before you can navigate to this page from your app, you need to reference this page's module in the
* global app router module. Add the following object to the global array of routes:
* { path: "Login", loadChildren: "./Login/Login.module#LoginModule" }
* Note that this simply points the path to the page module file. If you move the page, you need to update the route too.
*************************************************************/

@Component({
    selector: "Login",
    moduleId: module.id,
    templateUrl: "./Login.component.html"
})
export class LoginComponent implements OnInit {
    user: string;
    password: string;

    constructor(private router: Router) {
        /* ***********************************************************
        * Use the constructor to inject app services that you need in this component.
        *************************************************************/
    }

    ngOnInit(): void {
        /* ***********************************************************
        * Use the "ngOnInit" handler to initialize data for this component.
        *************************************************************/
    }

    onLoginWithSocialProviderButtonTap(): void {
        /* ***********************************************************
        * For log in with social provider you can add your custom logic or
        * use NativeScript plugin for log in with Facebook
        * http://market.nativescript.org/plugins/nativescript-facebook
        *************************************************************/
    }

    onSigninButtonTap(): void {
        const user = this.user;
        const password = this.password;
        // if(user != "admin" && password != "admin"){
        //     console.log("user: " + user + " Password:" + password);
        //     dialogs.alert("Usuario o contraseña incorrectos").then(()=> {
        //         this.user = "";
        //         this.password = "";
        //     });
        // }else{
            this.router.navigate(["Caja"]);
        // }
        /* ***********************************************************
        * Call your custom sign in logic using the email and password data.
        *************************************************************/
    }

    onForgotPasswordTap(): void {
        /* ***********************************************************
        * Call your Forgot Password logic here.
        *************************************************************/
    }
}
