"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ProfileComponent = /** @class */ (function () {
    function ProfileComponent() {
        this.profile = {
            name: 'Joe',
            password: 'bl0gs',
            angularPro: false,
            dob: new Date(),
            codingPower: 1,
            title: 'Amazing Results',
            score: 5,
            date: new Date(),
            component: 'DataForm',
            note: "This looks really great, \nI was really amazed how little effort it took to implement it.\nI can't wait to see other components",
            test: false
        };
    }
    ProfileComponent.prototype.save = function () {
        console.log(JSON.stringify(this.profile, null, 2));
    };
    ProfileComponent.prototype.clear = function () {
        this.profile.name = '';
        this.profile.password = '';
        this.profile.angularPro = false;
        this.profile.dob = new Date();
        this.profile.codingPower = 1;
    };
    ProfileComponent.prototype.clearForm = function () {
        this.profile = {
            name: '',
            password: '',
            angularPro: false,
            dob: new Date(),
            codingPower: 1,
            title: "",
            score: 5,
            date: new Date(),
            component: "",
            note: "",
            test: false,
        };
    };
    ProfileComponent = __decorate([
        core_1.Component({
            selector: 'my-profile',
            moduleId: module.id,
            templateUrl: './profile.component.html',
            styleUrls: ['./profile.component.css']
        }),
        __metadata("design:paramtypes", [])
    ], ProfileComponent);
    return ProfileComponent;
}());
exports.ProfileComponent = ProfileComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZmlsZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJwcm9maWxlLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEwQztBQVExQztJQUdFO1FBQ0UsSUFBSSxDQUFDLE9BQU8sR0FBRztZQUNiLElBQUksRUFBRSxLQUFLO1lBQ1gsUUFBUSxFQUFFLE9BQU87WUFDakIsVUFBVSxFQUFFLEtBQUs7WUFDakIsR0FBRyxFQUFFLElBQUksSUFBSSxFQUFFO1lBQ2YsV0FBVyxFQUFFLENBQUM7WUFDZCxLQUFLLEVBQUUsaUJBQWlCO1lBQzFCLEtBQUssRUFBRSxDQUFDO1lBQ1IsSUFBSSxFQUFFLElBQUksSUFBSSxFQUFFO1lBQ2hCLFNBQVMsRUFBRSxVQUFVO1lBQ3JCLElBQUksRUFBRSxpSUFFMkI7WUFDakMsSUFBSSxFQUFFLEtBQUs7U0FDVixDQUFBO0lBQ0gsQ0FBQztJQUVELCtCQUFJLEdBQUo7UUFDRSxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRUQsZ0NBQUssR0FBTDtRQUNFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztRQUN2QixJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDM0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ2hDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7UUFDOUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFFRCxvQ0FBUyxHQUFUO1FBQ0UsSUFBSSxDQUFDLE9BQU8sR0FBRztZQUNiLElBQUksRUFBRSxFQUFFO1lBQ1IsUUFBUSxFQUFFLEVBQUU7WUFDWixVQUFVLEVBQUUsS0FBSztZQUNqQixHQUFHLEVBQUUsSUFBSSxJQUFJLEVBQUU7WUFDZixXQUFXLEVBQUUsQ0FBQztZQUNkLEtBQUssRUFBRSxFQUFFO1lBQ1gsS0FBSyxFQUFFLENBQUM7WUFDUixJQUFJLEVBQUUsSUFBSSxJQUFJLEVBQUU7WUFDaEIsU0FBUyxFQUFFLEVBQUU7WUFDYixJQUFJLEVBQUUsRUFBRTtZQUNSLElBQUksRUFBRSxLQUFLO1NBQ1YsQ0FBQTtJQUNILENBQUM7SUEvQ1UsZ0JBQWdCO1FBTjVCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsWUFBWTtZQUN0QixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLDBCQUEwQjtZQUN2QyxTQUFTLEVBQUUsQ0FBQyx5QkFBeUIsQ0FBQztTQUN2QyxDQUFDOztPQUNXLGdCQUFnQixDQWdENUI7SUFBRCx1QkFBQztDQUFBLEFBaERELElBZ0RDO0FBaERZLDRDQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdteS1wcm9maWxlJyxcclxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9wcm9maWxlLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9wcm9maWxlLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgUHJvZmlsZUNvbXBvbmVudCB7XHJcbiAgcHJvZmlsZTogUHJvZmlsZTtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgICB0aGlzLnByb2ZpbGUgPSB7XHJcbiAgICAgIG5hbWU6ICdKb2UnLFxyXG4gICAgICBwYXNzd29yZDogJ2JsMGdzJyxcclxuICAgICAgYW5ndWxhclBybzogZmFsc2UsXHJcbiAgICAgIGRvYjogbmV3IERhdGUoKSxcclxuICAgICAgY29kaW5nUG93ZXI6IDEsXHJcbiAgICAgIHRpdGxlOiAnQW1hemluZyBSZXN1bHRzJyxcclxuICAgIHNjb3JlOiA1LFxyXG4gICAgZGF0ZTogbmV3IERhdGUoKSxcclxuICAgIGNvbXBvbmVudDogJ0RhdGFGb3JtJyxcclxuICAgIG5vdGU6IGBUaGlzIGxvb2tzIHJlYWxseSBncmVhdCwgXHJcbkkgd2FzIHJlYWxseSBhbWF6ZWQgaG93IGxpdHRsZSBlZmZvcnQgaXQgdG9vayB0byBpbXBsZW1lbnQgaXQuXHJcbkkgY2FuJ3Qgd2FpdCB0byBzZWUgb3RoZXIgY29tcG9uZW50c2AsXHJcbiAgICB0ZXN0OiBmYWxzZVxyXG4gICAgfVxyXG4gIH1cclxuICBcclxuICBzYXZlKCkge1xyXG4gICAgY29uc29sZS5sb2coSlNPTi5zdHJpbmdpZnkodGhpcy5wcm9maWxlLCBudWxsLCAyKSk7XHJcbiAgfVxyXG5cclxuICBjbGVhcigpIHtcclxuICAgIHRoaXMucHJvZmlsZS5uYW1lID0gJyc7XHJcbiAgICB0aGlzLnByb2ZpbGUucGFzc3dvcmQgPSAnJztcclxuICAgIHRoaXMucHJvZmlsZS5hbmd1bGFyUHJvID0gZmFsc2U7XHJcbiAgICB0aGlzLnByb2ZpbGUuZG9iID0gbmV3IERhdGUoKTtcclxuICAgIHRoaXMucHJvZmlsZS5jb2RpbmdQb3dlciA9IDE7XHJcbiAgfVxyXG5cclxuICBjbGVhckZvcm0oKSB7XHJcbiAgICB0aGlzLnByb2ZpbGUgPSB7XHJcbiAgICAgIG5hbWU6ICcnLFxyXG4gICAgICBwYXNzd29yZDogJycsXHJcbiAgICAgIGFuZ3VsYXJQcm86IGZhbHNlLFxyXG4gICAgICBkb2I6IG5ldyBEYXRlKCksXHJcbiAgICAgIGNvZGluZ1Bvd2VyOiAxLFxyXG4gICAgICB0aXRsZTogXCJcIixcclxuICAgIHNjb3JlOiA1LFxyXG4gICAgZGF0ZTogbmV3IERhdGUoKSxcclxuICAgIGNvbXBvbmVudDogXCJcIixcclxuICAgIG5vdGU6IFwiXCIsXHJcbiAgICB0ZXN0OiBmYWxzZSxcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgUHJvZmlsZSB7XHJcbiAgbmFtZTogc3RyaW5nO1xyXG4gIHBhc3N3b3JkOiBzdHJpbmc7XHJcbiAgYW5ndWxhclBybzogYm9vbGVhbjtcclxuICBkb2I6IERhdGU7XHJcbiAgY29kaW5nUG93ZXI6IG51bWJlcjtcclxuICB0aXRsZTogc3RyaW5nO1xyXG4gICAgc2NvcmU6IG51bWJlcjtcclxuICAgIGRhdGU6IERhdGU7XHJcbiAgICBjb21wb25lbnQ6IHN0cmluZztcclxuICAgIG5vdGU6IHN0cmluZzsgXHJcbiAgICB0ZXN0OiBib29sZWFuO1xyXG59Il19