"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var FootballFactory = /** @class */ (function () {
    function FootballFactory() {
    }
    FootballFactory.leagueTableFromRaw = function (rawLeagueTable) {
        var _this = this;
        // If table for a leage (not a tournament with groups), then append teamId for each standing.
        // This doesn't apply to tournaments, as tournament teams already contain the teamId value
        if (rawLeagueTable.standing) {
            rawLeagueTable.standing.forEach(function (standing) { return _this.appendStandingTeamId(standing); });
        }
        return rawLeagueTable;
    };
    FootballFactory.teamFromRaw = function (rawTeam) {
        this.appendTeamId(rawTeam);
        return rawTeam;
    };
    FootballFactory.teamsFromRaw = function (rawTeams) {
        var _this = this;
        return rawTeams.teams.map(function (rawTeam) { return _this.teamFromRaw(rawTeam); });
    };
    FootballFactory.fixtureFromRaw = function (rawFixture) {
        this.appendFixtureIds(rawFixture);
        return rawFixture;
    };
    FootballFactory.fixturesFromRaw = function (rawFixtures) {
        var _this = this;
        return rawFixtures.fixtures.map(function (rawFixture) { return _this.fixtureFromRaw(rawFixture); });
    };
    FootballFactory.playerFromRaw = function (rawPlayer) {
        return rawPlayer;
    };
    FootballFactory.playersFromRaw = function (rawPlayers) {
        var _this = this;
        return rawPlayers.players.map(function (rawPlayer) { return _this.playerFromRaw(rawPlayer); });
    };
    FootballFactory.standingFromRaw = function (rawStanding) {
        this.appendStandingTeamId(rawStanding);
        return rawStanding;
    };
    /**
     * Extracts and appends teamId from _links.self.href
     */
    FootballFactory.appendTeamId = function (team) {
        team.teamId = this.extractId(team, 'self');
    };
    /**
     ** Extracts and appends fixtureId from _links.self.href /n
     ** Extracts and appends competitionId from _links.competition.href
     ** Extracts and appends homeTeamId from _links.homeTeam.href
     ** Extracts and appends awayTeamId from _links.awayTeam.href
     */
    FootballFactory.appendFixtureIds = function (fixture) {
        fixture.fixtureId = this.extractId(fixture, 'self');
        fixture.competitionId = this.extractId(fixture, 'competition');
        fixture.homeTeamId = this.extractId(fixture, 'homeTeam');
        fixture.awayTeamId = this.extractId(fixture, 'awayTeam');
    };
    /**
     * Extracts and appends teamId from _links.team.href
     */
    FootballFactory.appendStandingTeamId = function (standing) {
        standing.teamId = this.extractId(standing, 'team');
    };
    FootballFactory.extractId = function (item, from) {
        var url = item._links[from].href;
        return +url.split('/').pop();
    };
    return FootballFactory;
}());
exports.FootballFactory = FootballFactory;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9vdGJhbGwtZmFjdG9yeS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImZvb3RiYWxsLWZhY3RvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFFQTtJQUFBO0lBa0ZBLENBQUM7SUFoRlEsa0NBQWtCLEdBQXpCLFVBQTBCLGNBQW1CO1FBQTdDLGlCQVVDO1FBVEMsNkZBQTZGO1FBQzdGLDBGQUEwRjtRQUMxRixJQUFJLGNBQWMsQ0FBQyxRQUFRLEVBQUU7WUFDM0IsY0FBYyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQzdCLFVBQUEsUUFBUSxJQUFJLE9BQUEsS0FBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxFQUFuQyxDQUFtQyxDQUNoRCxDQUFDO1NBQ0g7UUFFRCxPQUFPLGNBQWMsQ0FBQztJQUN4QixDQUFDO0lBRU0sMkJBQVcsR0FBbEIsVUFBbUIsT0FBWTtRQUM3QixJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzNCLE9BQU8sT0FBTyxDQUFDO0lBQ2pCLENBQUM7SUFFTSw0QkFBWSxHQUFuQixVQUFvQixRQUF3QjtRQUE1QyxpQkFJQztRQUhDLE9BQU8sUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQ3ZCLFVBQUEsT0FBTyxJQUFJLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsRUFBekIsQ0FBeUIsQ0FDckMsQ0FBQztJQUNKLENBQUM7SUFFTSw4QkFBYyxHQUFyQixVQUFzQixVQUFlO1FBQ25DLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNsQyxPQUFPLFVBQVUsQ0FBQztJQUNwQixDQUFDO0lBRU0sK0JBQWUsR0FBdEIsVUFBdUIsV0FBOEI7UUFBckQsaUJBSUM7UUFIQyxPQUFPLFdBQVcsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUM3QixVQUFBLFVBQVUsSUFBSSxPQUFBLEtBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLEVBQS9CLENBQStCLENBQzlDLENBQUM7SUFDSixDQUFDO0lBRU0sNkJBQWEsR0FBcEIsVUFBcUIsU0FBYztRQUNqQyxPQUFPLFNBQVMsQ0FBQztJQUNuQixDQUFDO0lBRU0sOEJBQWMsR0FBckIsVUFBc0IsVUFBNEI7UUFBbEQsaUJBSUM7UUFIQyxPQUFPLFVBQVUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUMzQixVQUFBLFNBQVMsSUFBSSxPQUFBLEtBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLEVBQTdCLENBQTZCLENBQzNDLENBQUM7SUFDSixDQUFDO0lBRU0sK0JBQWUsR0FBdEIsVUFBdUIsV0FBZ0I7UUFDckMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3ZDLE9BQU8sV0FBVyxDQUFDO0lBQ3JCLENBQUM7SUFFRDs7T0FFRztJQUNZLDRCQUFZLEdBQTNCLFVBQTRCLElBQVU7UUFDcEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDWSxnQ0FBZ0IsR0FBL0IsVUFBZ0MsT0FBZ0I7UUFDOUMsT0FBTyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztRQUNwRCxPQUFPLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLGFBQWEsQ0FBQyxDQUFDO1FBQy9ELE9BQU8sQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDekQsT0FBTyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxVQUFVLENBQUMsQ0FBQztJQUMzRCxDQUFDO0lBRUQ7O09BRUc7SUFDWSxvQ0FBb0IsR0FBbkMsVUFBb0MsUUFBa0I7UUFDcEQsUUFBUSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRWMseUJBQVMsR0FBeEIsVUFBeUIsSUFBUyxFQUFFLElBQVk7UUFDOUMsSUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDbkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7SUFDL0IsQ0FBQztJQUNILHNCQUFDO0FBQUQsQ0FBQyxBQWxGRCxJQWtGQztBQWxGWSwwQ0FBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBldGl0aW9uLCBMZWFndWVUYWJsZSwgVGVhbSwgRml4dHVyZSwgUGxheWVyLCBTdGFuZGluZyB9IGZyb20gJy4vJztcclxuXHJcbmV4cG9ydCBjbGFzcyBGb290YmFsbEZhY3Rvcnkge1xyXG5cclxuICBzdGF0aWMgbGVhZ3VlVGFibGVGcm9tUmF3KHJhd0xlYWd1ZVRhYmxlOiBhbnkpOiBMZWFndWVUYWJsZSB7XHJcbiAgICAvLyBJZiB0YWJsZSBmb3IgYSBsZWFnZSAobm90IGEgdG91cm5hbWVudCB3aXRoIGdyb3VwcyksIHRoZW4gYXBwZW5kIHRlYW1JZCBmb3IgZWFjaCBzdGFuZGluZy5cclxuICAgIC8vIFRoaXMgZG9lc24ndCBhcHBseSB0byB0b3VybmFtZW50cywgYXMgdG91cm5hbWVudCB0ZWFtcyBhbHJlYWR5IGNvbnRhaW4gdGhlIHRlYW1JZCB2YWx1ZVxyXG4gICAgaWYgKHJhd0xlYWd1ZVRhYmxlLnN0YW5kaW5nKSB7XHJcbiAgICAgIHJhd0xlYWd1ZVRhYmxlLnN0YW5kaW5nLmZvckVhY2goXHJcbiAgICAgICAgc3RhbmRpbmcgPT4gdGhpcy5hcHBlbmRTdGFuZGluZ1RlYW1JZChzdGFuZGluZylcclxuICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gcmF3TGVhZ3VlVGFibGU7XHJcbiAgfVxyXG5cclxuICBzdGF0aWMgdGVhbUZyb21SYXcocmF3VGVhbTogYW55KTogVGVhbSB7XHJcbiAgICB0aGlzLmFwcGVuZFRlYW1JZChyYXdUZWFtKTtcclxuICAgIHJldHVybiByYXdUZWFtO1xyXG4gIH1cclxuXHJcbiAgc3RhdGljIHRlYW1zRnJvbVJhdyhyYXdUZWFtczoge3RlYW1zOiBhbnlbXX0pOiBUZWFtW10ge1xyXG4gICAgcmV0dXJuIHJhd1RlYW1zLnRlYW1zLm1hcChcclxuICAgICAgcmF3VGVhbSA9PiB0aGlzLnRlYW1Gcm9tUmF3KHJhd1RlYW0pXHJcbiAgICApO1xyXG4gIH1cclxuXHJcbiAgc3RhdGljIGZpeHR1cmVGcm9tUmF3KHJhd0ZpeHR1cmU6IGFueSk6IEZpeHR1cmUge1xyXG4gICAgdGhpcy5hcHBlbmRGaXh0dXJlSWRzKHJhd0ZpeHR1cmUpO1xyXG4gICAgcmV0dXJuIHJhd0ZpeHR1cmU7XHJcbiAgfVxyXG5cclxuICBzdGF0aWMgZml4dHVyZXNGcm9tUmF3KHJhd0ZpeHR1cmVzOiB7Zml4dHVyZXM6IGFueVtdfSk6IEZpeHR1cmVbXSB7XHJcbiAgICByZXR1cm4gcmF3Rml4dHVyZXMuZml4dHVyZXMubWFwKFxyXG4gICAgICByYXdGaXh0dXJlID0+IHRoaXMuZml4dHVyZUZyb21SYXcocmF3Rml4dHVyZSlcclxuICAgICk7XHJcbiAgfVxyXG5cclxuICBzdGF0aWMgcGxheWVyRnJvbVJhdyhyYXdQbGF5ZXI6IGFueSk6IFBsYXllciB7XHJcbiAgICByZXR1cm4gcmF3UGxheWVyO1xyXG4gIH1cclxuXHJcbiAgc3RhdGljIHBsYXllcnNGcm9tUmF3KHJhd1BsYXllcnM6IHtwbGF5ZXJzOiBhbnlbXX0pOiBQbGF5ZXJbXSB7XHJcbiAgICByZXR1cm4gcmF3UGxheWVycy5wbGF5ZXJzLm1hcChcclxuICAgICAgcmF3UGxheWVyID0+IHRoaXMucGxheWVyRnJvbVJhdyhyYXdQbGF5ZXIpXHJcbiAgICApO1xyXG4gIH1cclxuXHJcbiAgc3RhdGljIHN0YW5kaW5nRnJvbVJhdyhyYXdTdGFuZGluZzogYW55KTogU3RhbmRpbmcge1xyXG4gICAgdGhpcy5hcHBlbmRTdGFuZGluZ1RlYW1JZChyYXdTdGFuZGluZyk7XHJcbiAgICByZXR1cm4gcmF3U3RhbmRpbmc7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBFeHRyYWN0cyBhbmQgYXBwZW5kcyB0ZWFtSWQgZnJvbSBfbGlua3Muc2VsZi5ocmVmXHJcbiAgICovXHJcbiAgcHJpdmF0ZSBzdGF0aWMgYXBwZW5kVGVhbUlkKHRlYW06IFRlYW0pIHtcclxuICAgIHRlYW0udGVhbUlkID0gdGhpcy5leHRyYWN0SWQodGVhbSwgJ3NlbGYnKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqKiBFeHRyYWN0cyBhbmQgYXBwZW5kcyBmaXh0dXJlSWQgZnJvbSBfbGlua3Muc2VsZi5ocmVmIC9uXHJcbiAgICoqIEV4dHJhY3RzIGFuZCBhcHBlbmRzIGNvbXBldGl0aW9uSWQgZnJvbSBfbGlua3MuY29tcGV0aXRpb24uaHJlZlxyXG4gICAqKiBFeHRyYWN0cyBhbmQgYXBwZW5kcyBob21lVGVhbUlkIGZyb20gX2xpbmtzLmhvbWVUZWFtLmhyZWZcclxuICAgKiogRXh0cmFjdHMgYW5kIGFwcGVuZHMgYXdheVRlYW1JZCBmcm9tIF9saW5rcy5hd2F5VGVhbS5ocmVmXHJcbiAgICovXHJcbiAgcHJpdmF0ZSBzdGF0aWMgYXBwZW5kRml4dHVyZUlkcyhmaXh0dXJlOiBGaXh0dXJlKSB7XHJcbiAgICBmaXh0dXJlLmZpeHR1cmVJZCA9IHRoaXMuZXh0cmFjdElkKGZpeHR1cmUsICdzZWxmJyk7XHJcbiAgICBmaXh0dXJlLmNvbXBldGl0aW9uSWQgPSB0aGlzLmV4dHJhY3RJZChmaXh0dXJlLCAnY29tcGV0aXRpb24nKTtcclxuICAgIGZpeHR1cmUuaG9tZVRlYW1JZCA9IHRoaXMuZXh0cmFjdElkKGZpeHR1cmUsICdob21lVGVhbScpO1xyXG4gICAgZml4dHVyZS5hd2F5VGVhbUlkID0gdGhpcy5leHRyYWN0SWQoZml4dHVyZSwgJ2F3YXlUZWFtJyk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBFeHRyYWN0cyBhbmQgYXBwZW5kcyB0ZWFtSWQgZnJvbSBfbGlua3MudGVhbS5ocmVmXHJcbiAgICovXHJcbiAgcHJpdmF0ZSBzdGF0aWMgYXBwZW5kU3RhbmRpbmdUZWFtSWQoc3RhbmRpbmc6IFN0YW5kaW5nKSB7XHJcbiAgICBzdGFuZGluZy50ZWFtSWQgPSB0aGlzLmV4dHJhY3RJZChzdGFuZGluZywgJ3RlYW0nKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgc3RhdGljIGV4dHJhY3RJZChpdGVtOiBhbnksIGZyb206IHN0cmluZykge1xyXG4gICAgY29uc3QgdXJsID0gaXRlbS5fbGlua3NbZnJvbV0uaHJlZjtcclxuICAgIHJldHVybiArdXJsLnNwbGl0KCcvJykucG9wKCk7XHJcbiAgfVxyXG59Il19