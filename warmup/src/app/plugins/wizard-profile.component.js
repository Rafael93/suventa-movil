"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var image_source_1 = require("image-source");
var WizardProfileComponent = /** @class */ (function () {
    function WizardProfileComponent() {
        this.name = 'Sebastian';
        this.powers = [
            { name: 'Cosmic Manipulation', level: 1, description: 'The power to manipulate all cosmic forces.' },
            { name: 'Magic Resistance', level: 1, description: 'The power to be highly resistant to Magic. Not to be confused with Magic Immunity.' },
            { name: 'Telekinesis', level: 1, description: 'User can influence/manipulate/move objects/matter with their mind.' },
            { name: 'Alchemy', level: 1, description: 'The mystic and scientific pursuit of the power of the Philosopher\'s Stone and Universal Panacea.' },
            { name: 'Invocation', level: 1, description: 'The ability to gain control over the target\'s life or actions by speaking their name or phrases.' },
            { name: 'Magic Detection', level: 1, description: 'The ability to sense the presence of magic in one\'s vicinity.' },
            { name: 'Fiction Manipulation', level: 1, description: 'The power to manipulate anything invented through imaginative and theoretical ideologies.' }
        ];
    }
    WizardProfileComponent.prototype.ngOnInit = function () {
        // get camera permissions when loading for the first time
        this.reloadPowers();
    };
    WizardProfileComponent.prototype.share = function () {
        var messageBody = "name: " + this.name + ", powers: " + JSON.stringify(this.powers);
        // Add social shareText code here
    };
    WizardProfileComponent.prototype.sharePicture = function () {
        if (this.profilePicture) {
            // Call SocialShare.shareImage here
        }
    };
    WizardProfileComponent.prototype.takeProfilePicture = function () {
        // call camera.takePicture here
    };
    WizardProfileComponent.prototype.updateProfilePicture = function (asset) {
        var _this = this;
        var imageSource = new image_source_1.ImageSource();
        imageSource.fromAsset(asset)
            .then(function (image) {
            _this.profilePicture = image;
        });
    };
    WizardProfileComponent.prototype.onPull = function (args) {
        this.reloadPowers();
        args.object.refreshing = false;
    };
    WizardProfileComponent.prototype.reloadPowers = function () {
        this.powers.forEach(function (power) { return power.level = Math.round(Math.random() * 10); });
    };
    WizardProfileComponent.prototype.onPowerTap = function (event) {
        this.displayPower(this.powers[event.index]);
    };
    WizardProfileComponent.prototype.displayPower = function (power) {
        if (power.level < 5) {
            alert(power.name + ' ' + power.description + ' ' + 'Nice');
        }
        else if (power.level < 9) {
            alert(power.name + ' ' + power.description + ' ' + 'W00000W!!!');
        }
        else {
            alert(power.name + ' ' + power.description + ' ' + 'Be careful');
        }
    };
    WizardProfileComponent = __decorate([
        core_1.Component({
            selector: 'my-share',
            moduleId: module.id,
            templateUrl: './wizard-profile.component.html'
        }),
        __metadata("design:paramtypes", [])
    ], WizardProfileComponent);
    return WizardProfileComponent;
}());
exports.WizardProfileComponent = WizardProfileComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2l6YXJkLXByb2ZpbGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsid2l6YXJkLXByb2ZpbGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBRWxELDZDQUEyQztBQVEzQztJQWVFO1FBZE8sU0FBSSxHQUFXLFdBQVcsQ0FBQztRQUUzQixXQUFNLEdBQVk7WUFDdkIsRUFBRSxJQUFJLEVBQUUscUJBQXFCLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxXQUFXLEVBQUUsNENBQTRDLEVBQUM7WUFDbkcsRUFBRSxJQUFJLEVBQUUsa0JBQWtCLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxXQUFXLEVBQUUsb0ZBQW9GLEVBQUM7WUFDeEksRUFBRSxJQUFJLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsV0FBVyxFQUFFLG9FQUFvRSxFQUFDO1lBQ25ILEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLFdBQVcsRUFBRSxtR0FBbUcsRUFBQztZQUM5SSxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxXQUFXLEVBQUUsbUdBQW1HLEVBQUM7WUFDakosRUFBRSxJQUFJLEVBQUUsaUJBQWlCLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxXQUFXLEVBQUUsZ0VBQWdFLEVBQUM7WUFDbkgsRUFBRSxJQUFJLEVBQUUsc0JBQXNCLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxXQUFXLEVBQUUsMkZBQTJGLEVBQUM7U0FDcEosQ0FBQztJQUtGLENBQUM7SUFFRCx5Q0FBUSxHQUFSO1FBQ0UseURBQXlEO1FBR3pELElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUN0QixDQUFDO0lBRUQsc0NBQUssR0FBTDtRQUNFLElBQU0sV0FBVyxHQUFHLFdBQVMsSUFBSSxDQUFDLElBQUksa0JBQWEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFHLENBQUM7UUFFakYsaUNBQWlDO0lBRW5DLENBQUM7SUFFRCw2Q0FBWSxHQUFaO1FBQ0UsSUFBRyxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3RCLG1DQUFtQztTQUVwQztJQUNILENBQUM7SUFFRCxtREFBa0IsR0FBbEI7UUFDRSwrQkFBK0I7SUFDakMsQ0FBQztJQUVELHFEQUFvQixHQUFwQixVQUFxQixLQUFpQjtRQUF0QyxpQkFNQztRQUxDLElBQU0sV0FBVyxHQUFHLElBQUksMEJBQVcsRUFBRSxDQUFDO1FBQ3RDLFdBQVcsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDO2FBQ3pCLElBQUksQ0FBQyxVQUFBLEtBQUs7WUFDVCxLQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztRQUM5QixDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUM7SUFFRCx1Q0FBTSxHQUFOLFVBQU8sSUFBSTtRQUNULElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUVwQixJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7SUFDakMsQ0FBQztJQUVELDZDQUFZLEdBQVo7UUFDRSxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FDakIsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFLLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFDLEVBQUUsQ0FBQyxFQUExQyxDQUEwQyxDQUNwRCxDQUFDO0lBQ0osQ0FBQztJQUVELDJDQUFVLEdBQVYsVUFBVyxLQUFLO1FBQ2QsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFFRCw2Q0FBWSxHQUFaLFVBQWEsS0FBWTtRQUN2QixJQUFHLEtBQUssQ0FBQyxLQUFLLEdBQUcsQ0FBQyxFQUFFO1lBQ2xCLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLEdBQUcsR0FBRyxLQUFLLENBQUMsV0FBVyxHQUFHLEdBQUcsR0FBRyxNQUFNLENBQUMsQ0FBQztTQUM1RDthQUFNLElBQUcsS0FBSyxDQUFDLEtBQUssR0FBRyxDQUFDLEVBQUU7WUFDekIsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsR0FBRyxHQUFHLEtBQUssQ0FBQyxXQUFXLEdBQUcsR0FBRyxHQUFHLFlBQVksQ0FBQyxDQUFDO1NBQ2xFO2FBQU07WUFDTCxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDLFdBQVcsR0FBRyxHQUFHLEdBQUcsWUFBWSxDQUFDLENBQUM7U0FDbEU7SUFDSCxDQUFDO0lBM0VVLHNCQUFzQjtRQUxsQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLFVBQVU7WUFDcEIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSxpQ0FBaUM7U0FDL0MsQ0FBQzs7T0FDVyxzQkFBc0IsQ0E0RWxDO0lBQUQsNkJBQUM7Q0FBQSxBQTVFRCxJQTRFQztBQTVFWSx3REFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBJbWFnZUFzc2V0IH0gZnJvbSAnaW1hZ2UtYXNzZXQnO1xyXG5pbXBvcnQgeyBJbWFnZVNvdXJjZSB9IGZyb20gJ2ltYWdlLXNvdXJjZSc7XHJcbiBcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbXktc2hhcmUnLFxyXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL3dpemFyZC1wcm9maWxlLmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgV2l6YXJkUHJvZmlsZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdHtcclxuICBwdWJsaWMgbmFtZTogc3RyaW5nID0gJ1NlYmFzdGlhbic7XHJcblxyXG4gIHB1YmxpYyBwb3dlcnM6IFBvd2VyW10gPSBbXHJcbiAgICB7IG5hbWU6ICdDb3NtaWMgTWFuaXB1bGF0aW9uJywgbGV2ZWw6IDEsIGRlc2NyaXB0aW9uOiAnVGhlIHBvd2VyIHRvIG1hbmlwdWxhdGUgYWxsIGNvc21pYyBmb3JjZXMuJ30sXHJcbiAgICB7IG5hbWU6ICdNYWdpYyBSZXNpc3RhbmNlJywgbGV2ZWw6IDEsIGRlc2NyaXB0aW9uOiAnVGhlIHBvd2VyIHRvIGJlIGhpZ2hseSByZXNpc3RhbnQgdG8gTWFnaWMuIE5vdCB0byBiZSBjb25mdXNlZCB3aXRoIE1hZ2ljIEltbXVuaXR5Lid9LFxyXG4gICAgeyBuYW1lOiAnVGVsZWtpbmVzaXMnLCBsZXZlbDogMSwgZGVzY3JpcHRpb246ICdVc2VyIGNhbiBpbmZsdWVuY2UvbWFuaXB1bGF0ZS9tb3ZlIG9iamVjdHMvbWF0dGVyIHdpdGggdGhlaXIgbWluZC4nfSxcclxuICAgIHsgbmFtZTogJ0FsY2hlbXknLCBsZXZlbDogMSwgZGVzY3JpcHRpb246ICdUaGUgbXlzdGljIGFuZCBzY2llbnRpZmljIHB1cnN1aXQgb2YgdGhlIHBvd2VyIG9mIHRoZSBQaGlsb3NvcGhlclxcJ3MgU3RvbmUgYW5kIFVuaXZlcnNhbCBQYW5hY2VhLid9LFxyXG4gICAgeyBuYW1lOiAnSW52b2NhdGlvbicsIGxldmVsOiAxLCBkZXNjcmlwdGlvbjogJ1RoZSBhYmlsaXR5IHRvIGdhaW4gY29udHJvbCBvdmVyIHRoZSB0YXJnZXRcXCdzIGxpZmUgb3IgYWN0aW9ucyBieSBzcGVha2luZyB0aGVpciBuYW1lIG9yIHBocmFzZXMuJ30sXHJcbiAgICB7IG5hbWU6ICdNYWdpYyBEZXRlY3Rpb24nLCBsZXZlbDogMSwgZGVzY3JpcHRpb246ICdUaGUgYWJpbGl0eSB0byBzZW5zZSB0aGUgcHJlc2VuY2Ugb2YgbWFnaWMgaW4gb25lXFwncyB2aWNpbml0eS4nfSxcclxuICAgIHsgbmFtZTogJ0ZpY3Rpb24gTWFuaXB1bGF0aW9uJywgbGV2ZWw6IDEsIGRlc2NyaXB0aW9uOiAnVGhlIHBvd2VyIHRvIG1hbmlwdWxhdGUgYW55dGhpbmcgaW52ZW50ZWQgdGhyb3VnaCBpbWFnaW5hdGl2ZSBhbmQgdGhlb3JldGljYWwgaWRlb2xvZ2llcy4nfVxyXG4gIF07XHJcbiAgXHJcbiAgcHVibGljIHByb2ZpbGVQaWN0dXJlOiBJbWFnZVNvdXJjZTtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICAgIC8vIGdldCBjYW1lcmEgcGVybWlzc2lvbnMgd2hlbiBsb2FkaW5nIGZvciB0aGUgZmlyc3QgdGltZVxyXG4gICAgXHJcblxyXG4gICAgdGhpcy5yZWxvYWRQb3dlcnMoKTtcclxuICB9XHJcblxyXG4gIHNoYXJlKCkge1xyXG4gICAgY29uc3QgbWVzc2FnZUJvZHkgPSBgbmFtZTogJHt0aGlzLm5hbWV9LCBwb3dlcnM6ICR7SlNPTi5zdHJpbmdpZnkodGhpcy5wb3dlcnMpfWA7XHJcblxyXG4gICAgLy8gQWRkIHNvY2lhbCBzaGFyZVRleHQgY29kZSBoZXJlXHJcbiAgICBcclxuICB9XHJcblxyXG4gIHNoYXJlUGljdHVyZSgpIHtcclxuICAgIGlmKHRoaXMucHJvZmlsZVBpY3R1cmUpIHtcclxuICAgICAgLy8gQ2FsbCBTb2NpYWxTaGFyZS5zaGFyZUltYWdlIGhlcmVcclxuXHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICB0YWtlUHJvZmlsZVBpY3R1cmUoKSB7XHJcbiAgICAvLyBjYWxsIGNhbWVyYS50YWtlUGljdHVyZSBoZXJlXHJcbiAgfVxyXG5cclxuICB1cGRhdGVQcm9maWxlUGljdHVyZShhc3NldDogSW1hZ2VBc3NldCkge1xyXG4gICAgY29uc3QgaW1hZ2VTb3VyY2UgPSBuZXcgSW1hZ2VTb3VyY2UoKTtcclxuICAgIGltYWdlU291cmNlLmZyb21Bc3NldChhc3NldClcclxuICAgICAgLnRoZW4oaW1hZ2UgPT4ge1xyXG4gICAgICAgIHRoaXMucHJvZmlsZVBpY3R1cmUgPSBpbWFnZTtcclxuICAgICAgfSlcclxuICB9XHJcblxyXG4gIG9uUHVsbChhcmdzKSB7XHJcbiAgICB0aGlzLnJlbG9hZFBvd2VycygpO1xyXG4gICAgXHJcbiAgICBhcmdzLm9iamVjdC5yZWZyZXNoaW5nID0gZmFsc2U7XHJcbiAgfVxyXG5cclxuICByZWxvYWRQb3dlcnMoKSB7XHJcbiAgICB0aGlzLnBvd2Vycy5mb3JFYWNoKFxyXG4gICAgICBwb3dlciA9PiBwb3dlci5sZXZlbCA9IE1hdGgucm91bmQoTWF0aC5yYW5kb20oKSoxMClcclxuICAgICk7XHJcbiAgfVxyXG5cclxuICBvblBvd2VyVGFwKGV2ZW50KSB7XHJcbiAgICB0aGlzLmRpc3BsYXlQb3dlcih0aGlzLnBvd2Vyc1tldmVudC5pbmRleF0pO1xyXG4gIH1cclxuXHJcbiAgZGlzcGxheVBvd2VyKHBvd2VyOiBQb3dlcikge1xyXG4gICAgaWYocG93ZXIubGV2ZWwgPCA1KSB7XHJcbiAgICAgIGFsZXJ0KHBvd2VyLm5hbWUgKyAnICcgKyBwb3dlci5kZXNjcmlwdGlvbiArICcgJyArICdOaWNlJyk7XHJcbiAgICB9IGVsc2UgaWYocG93ZXIubGV2ZWwgPCA5KSB7XHJcbiAgICAgIGFsZXJ0KHBvd2VyLm5hbWUgKyAnICcgKyBwb3dlci5kZXNjcmlwdGlvbiArICcgJyArICdXMDAwMDBXISEhJyk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBhbGVydChwb3dlci5uYW1lICsgJyAnICsgcG93ZXIuZGVzY3JpcHRpb24gKyAnICcgKyAnQmUgY2FyZWZ1bCcpO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBQb3dlciB7XHJcbiAgbmFtZTogc3RyaW5nO1xyXG4gIGxldmVsOiBudW1iZXI7XHJcbiAgZGVzY3JpcHRpb24/OiBzdHJpbmc7XHJcbn1cclxuIl19