"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var app_routing_module_1 = require("./app-routing.module");
var app_component_1 = require("./app.component");
// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from 'nativescript-angular/forms';
// Uncomment and add to NgModule imports if you need to use the HttpClient wrapper
// import { NativeScriptHttpClientModule } from 'nativescript-angular/http-client';
var angular_1 = require("nativescript-ui-dataform/angular");
var forms_1 = require("nativescript-angular/forms");
var profile_component_1 = require("./profile/profile.component");
var color_1 = require("./color");
var service_test_component_1 = require("./service-test/service-test.component");
var tables_component_1 = require("./football/tables.component");
var league_table_component_1 = require("./football/league-table.component");
var competition_fixtures_component_1 = require("./football/competition-fixtures.component");
var fixture_component_1 = require("./football/fixture.component");
var team_component_1 = require("./football/team.component");
var player_component_1 = require("./football/player.component");
var wizard_profile_component_1 = require("./plugins/wizard-profile.component");
var Login_component_1 = require("./Login/Login.component");
var Caja_component_1 = require("./Caja/Caja.component");
var Productos_component_1 = require("./Productos/Productos.component");
var Inventario_component_1 = require("./Inventario/Inventario.component");
var Clientes_component_1 = require("./Clientes/Clientes.component");
var Reportes_component_1 = require("./Reportes/Reportes.component");
var AppModule = /** @class */ (function () {
    /*
    Pass your application module to the bootstrapModule function located in main.ts to start your app
    */
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            bootstrap: [
                app_component_1.AppComponent
            ],
            imports: [
                nativescript_module_1.NativeScriptModule,
                app_routing_module_1.AppRoutingModule,
                forms_1.NativeScriptFormsModule,
                //    NativeScriptHttpClientModule,
                angular_1.NativeScriptUIDataFormModule
            ],
            declarations: [
                app_component_1.AppComponent,
                //Lesson 1
                profile_component_1.ProfileComponent,
                //Lesson 2
                color_1.ColorComponent,
                color_1.BlueComponent,
                color_1.RedComponent,
                color_1.RGBComponent,
                //Lesson 3
                // Services
                service_test_component_1.ServiceTestComponent,
                // Components
                tables_component_1.TablesComponent,
                league_table_component_1.LeagueTableComponent,
                competition_fixtures_component_1.CompetitionFixturesComponent,
                fixture_component_1.FixtureComponent,
                team_component_1.TeamComponent,
                player_component_1.PlayerComponent,
                //Lesson 4
                wizard_profile_component_1.WizardProfileComponent,
                //Login
                Login_component_1.LoginComponent,
                //Caja
                Caja_component_1.CajaComponent,
                //Productos
                Productos_component_1.ProductosComponent,
                //Inventario
                Inventario_component_1.InventarioComponent,
                //Clientes
                Clientes_component_1.ClientesComponent,
                //Reportes
                Reportes_component_1.ReportesComponent
            ],
            providers: [],
            schemas: [
                core_1.NO_ERRORS_SCHEMA
            ]
        })
        /*
        Pass your application module to the bootstrapModule function located in main.ts to start your app
        */
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkQ7QUFDM0QsZ0ZBQThFO0FBRTlFLDJEQUF3RDtBQUN4RCxpREFBK0M7QUFFL0MsMkVBQTJFO0FBQzNFLHdFQUF3RTtBQUV4RSxrRkFBa0Y7QUFDbEYsbUZBQW1GO0FBRW5GLDREQUFnRjtBQUNoRixvREFBcUU7QUFFckUsaUVBQStEO0FBQy9ELGlDQUFvRjtBQUNwRixnRkFBNkU7QUFDN0UsZ0VBQThEO0FBQzlELDRFQUF5RTtBQUN6RSw0RkFBeUY7QUFDekYsa0VBQWdFO0FBQ2hFLDREQUEwRDtBQUMxRCxnRUFBOEQ7QUFDOUQsK0VBQTRFO0FBQzVFLDJEQUF5RDtBQUN6RCx3REFBc0Q7QUFDdEQsdUVBQXFFO0FBQ3JFLDBFQUF3RTtBQUN4RSxvRUFBa0U7QUFDbEUsb0VBQWtFO0FBb0VsRTtJQUhBOztNQUVFO0lBQ0Y7SUFBeUIsQ0FBQztJQUFiLFNBQVM7UUFsRXJCLGVBQVEsQ0FBQztZQUNOLFNBQVMsRUFBRTtnQkFDUCw0QkFBWTthQUNmO1lBQ0QsT0FBTyxFQUFFO2dCQUNMLHdDQUFrQjtnQkFDbEIscUNBQWdCO2dCQUVkLCtCQUF1QjtnQkFDekIsbUNBQW1DO2dCQUVuQyxzQ0FBNEI7YUFDL0I7WUFDRCxZQUFZLEVBQUU7Z0JBQ1YsNEJBQVk7Z0JBRVosVUFBVTtnQkFDVixvQ0FBZ0I7Z0JBRWhCLFVBQVU7Z0JBQ1Ysc0JBQWM7Z0JBQ2QscUJBQWE7Z0JBQ2Isb0JBQVk7Z0JBQ1osb0JBQVk7Z0JBRVosVUFBVTtnQkFDVixXQUFXO2dCQUNYLDZDQUFvQjtnQkFDcEIsYUFBYTtnQkFDYixrQ0FBZTtnQkFDZiw2Q0FBb0I7Z0JBQ3BCLDZEQUE0QjtnQkFDNUIsb0NBQWdCO2dCQUNoQiw4QkFBYTtnQkFDYixrQ0FBZTtnQkFFZixVQUFVO2dCQUNWLGlEQUFzQjtnQkFFdEIsT0FBTztnQkFDUCxnQ0FBYztnQkFFZCxNQUFNO2dCQUNOLDhCQUFhO2dCQUViLFdBQVc7Z0JBQ1gsd0NBQWtCO2dCQUVsQixZQUFZO2dCQUNaLDBDQUFtQjtnQkFFbkIsVUFBVTtnQkFDVixzQ0FBaUI7Z0JBRWpCLFVBQVU7Z0JBQ1Ysc0NBQWlCO2FBQ3BCO1lBQ0QsU0FBUyxFQUFFLEVBQ1Y7WUFDRCxPQUFPLEVBQUU7Z0JBQ0wsdUJBQWdCO2FBQ25CO1NBQ0osQ0FBQztRQUNGOztVQUVFO09BQ1csU0FBUyxDQUFJO0lBQUQsZ0JBQUM7Q0FBQSxBQUExQixJQUEwQjtBQUFiLDhCQUFTIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUsIE5PX0VSUk9SU19TQ0hFTUEgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0TW9kdWxlIH0gZnJvbSAnbmF0aXZlc2NyaXB0LWFuZ3VsYXIvbmF0aXZlc2NyaXB0Lm1vZHVsZSc7XHJcblxyXG5pbXBvcnQgeyBBcHBSb3V0aW5nTW9kdWxlIH0gZnJvbSAnLi9hcHAtcm91dGluZy5tb2R1bGUnO1xyXG5pbXBvcnQgeyBBcHBDb21wb25lbnQgfSBmcm9tICcuL2FwcC5jb21wb25lbnQnO1xyXG5cclxuLy8gVW5jb21tZW50IGFuZCBhZGQgdG8gTmdNb2R1bGUgaW1wb3J0cyBpZiB5b3UgbmVlZCB0byB1c2UgdHdvLXdheSBiaW5kaW5nXHJcbi8vIGltcG9ydCB7IE5hdGl2ZVNjcmlwdEZvcm1zTW9kdWxlIH0gZnJvbSAnbmF0aXZlc2NyaXB0LWFuZ3VsYXIvZm9ybXMnO1xyXG5cclxuLy8gVW5jb21tZW50IGFuZCBhZGQgdG8gTmdNb2R1bGUgaW1wb3J0cyBpZiB5b3UgbmVlZCB0byB1c2UgdGhlIEh0dHBDbGllbnQgd3JhcHBlclxyXG4vLyBpbXBvcnQgeyBOYXRpdmVTY3JpcHRIdHRwQ2xpZW50TW9kdWxlIH0gZnJvbSAnbmF0aXZlc2NyaXB0LWFuZ3VsYXIvaHR0cC1jbGllbnQnO1xyXG5cclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0VUlEYXRhRm9ybU1vZHVsZSB9IGZyb20gJ25hdGl2ZXNjcmlwdC11aS1kYXRhZm9ybS9hbmd1bGFyJztcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0Rm9ybXNNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvZm9ybXNcIjtcclxuXHJcbmltcG9ydCB7IFByb2ZpbGVDb21wb25lbnQgfSBmcm9tICcuL3Byb2ZpbGUvcHJvZmlsZS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDb2xvckNvbXBvbmVudCwgQmx1ZUNvbXBvbmVudCwgUmVkQ29tcG9uZW50LCBSR0JDb21wb25lbnQgfSBmcm9tICcuL2NvbG9yJztcclxuaW1wb3J0IHsgU2VydmljZVRlc3RDb21wb25lbnQgfSBmcm9tICcuL3NlcnZpY2UtdGVzdC9zZXJ2aWNlLXRlc3QuY29tcG9uZW50JztcclxuaW1wb3J0IHsgVGFibGVzQ29tcG9uZW50IH0gZnJvbSAnLi9mb290YmFsbC90YWJsZXMuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTGVhZ3VlVGFibGVDb21wb25lbnQgfSBmcm9tICcuL2Zvb3RiYWxsL2xlYWd1ZS10YWJsZS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDb21wZXRpdGlvbkZpeHR1cmVzQ29tcG9uZW50IH0gZnJvbSAnLi9mb290YmFsbC9jb21wZXRpdGlvbi1maXh0dXJlcy5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBGaXh0dXJlQ29tcG9uZW50IH0gZnJvbSAnLi9mb290YmFsbC9maXh0dXJlLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFRlYW1Db21wb25lbnQgfSBmcm9tICcuL2Zvb3RiYWxsL3RlYW0uY29tcG9uZW50JztcclxuaW1wb3J0IHsgUGxheWVyQ29tcG9uZW50IH0gZnJvbSAnLi9mb290YmFsbC9wbGF5ZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgV2l6YXJkUHJvZmlsZUNvbXBvbmVudCB9IGZyb20gJy4vcGx1Z2lucy93aXphcmQtcHJvZmlsZS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBMb2dpbkNvbXBvbmVudCB9IGZyb20gXCIuL0xvZ2luL0xvZ2luLmNvbXBvbmVudFwiO1xyXG5pbXBvcnQgeyBDYWphQ29tcG9uZW50IH0gZnJvbSBcIi4vQ2FqYS9DYWphLmNvbXBvbmVudFwiO1xyXG5pbXBvcnQgeyBQcm9kdWN0b3NDb21wb25lbnQgfSBmcm9tIFwiLi9Qcm9kdWN0b3MvUHJvZHVjdG9zLmNvbXBvbmVudFwiO1xyXG5pbXBvcnQgeyBJbnZlbnRhcmlvQ29tcG9uZW50IH0gZnJvbSBcIi4vSW52ZW50YXJpby9JbnZlbnRhcmlvLmNvbXBvbmVudFwiO1xyXG5pbXBvcnQgeyBDbGllbnRlc0NvbXBvbmVudCB9IGZyb20gXCIuL0NsaWVudGVzL0NsaWVudGVzLmNvbXBvbmVudFwiO1xyXG5pbXBvcnQgeyBSZXBvcnRlc0NvbXBvbmVudCB9IGZyb20gXCIuL1JlcG9ydGVzL1JlcG9ydGVzLmNvbXBvbmVudFwiO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGJvb3RzdHJhcDogW1xyXG4gICAgICAgIEFwcENvbXBvbmVudFxyXG4gICAgXSxcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBOYXRpdmVTY3JpcHRNb2R1bGUsXHJcbiAgICAgICAgQXBwUm91dGluZ01vZHVsZSxcclxuXHJcbiAgICAgICAgICBOYXRpdmVTY3JpcHRGb3Jtc01vZHVsZSxcclxuICAgICAgICAvLyAgICBOYXRpdmVTY3JpcHRIdHRwQ2xpZW50TW9kdWxlLFxyXG5cclxuICAgICAgICBOYXRpdmVTY3JpcHRVSURhdGFGb3JtTW9kdWxlXHJcbiAgICBdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgQXBwQ29tcG9uZW50LFxyXG5cclxuICAgICAgICAvL0xlc3NvbiAxXHJcbiAgICAgICAgUHJvZmlsZUNvbXBvbmVudCxcclxuXHJcbiAgICAgICAgLy9MZXNzb24gMlxyXG4gICAgICAgIENvbG9yQ29tcG9uZW50LFxyXG4gICAgICAgIEJsdWVDb21wb25lbnQsXHJcbiAgICAgICAgUmVkQ29tcG9uZW50LFxyXG4gICAgICAgIFJHQkNvbXBvbmVudCxcclxuXHJcbiAgICAgICAgLy9MZXNzb24gM1xyXG4gICAgICAgIC8vIFNlcnZpY2VzXHJcbiAgICAgICAgU2VydmljZVRlc3RDb21wb25lbnQsXHJcbiAgICAgICAgLy8gQ29tcG9uZW50c1xyXG4gICAgICAgIFRhYmxlc0NvbXBvbmVudCxcclxuICAgICAgICBMZWFndWVUYWJsZUNvbXBvbmVudCxcclxuICAgICAgICBDb21wZXRpdGlvbkZpeHR1cmVzQ29tcG9uZW50LFxyXG4gICAgICAgIEZpeHR1cmVDb21wb25lbnQsXHJcbiAgICAgICAgVGVhbUNvbXBvbmVudCxcclxuICAgICAgICBQbGF5ZXJDb21wb25lbnQsXHJcblxyXG4gICAgICAgIC8vTGVzc29uIDRcclxuICAgICAgICBXaXphcmRQcm9maWxlQ29tcG9uZW50LFxyXG4gICAgICAgIFxyXG4gICAgICAgIC8vTG9naW5cclxuICAgICAgICBMb2dpbkNvbXBvbmVudCxcclxuXHJcbiAgICAgICAgLy9DYWphXHJcbiAgICAgICAgQ2FqYUNvbXBvbmVudCxcclxuXHJcbiAgICAgICAgLy9Qcm9kdWN0b3NcclxuICAgICAgICBQcm9kdWN0b3NDb21wb25lbnQsXHJcblxyXG4gICAgICAgIC8vSW52ZW50YXJpb1xyXG4gICAgICAgIEludmVudGFyaW9Db21wb25lbnQsXHJcblxyXG4gICAgICAgIC8vQ2xpZW50ZXNcclxuICAgICAgICBDbGllbnRlc0NvbXBvbmVudCxcclxuXHJcbiAgICAgICAgLy9SZXBvcnRlc1xyXG4gICAgICAgIFJlcG9ydGVzQ29tcG9uZW50XHJcbiAgICBdLFxyXG4gICAgcHJvdmlkZXJzOiBbXHJcbiAgICBdLFxyXG4gICAgc2NoZW1hczogW1xyXG4gICAgICAgIE5PX0VSUk9SU19TQ0hFTUFcclxuICAgIF1cclxufSlcclxuLypcclxuUGFzcyB5b3VyIGFwcGxpY2F0aW9uIG1vZHVsZSB0byB0aGUgYm9vdHN0cmFwTW9kdWxlIGZ1bmN0aW9uIGxvY2F0ZWQgaW4gbWFpbi50cyB0byBzdGFydCB5b3VyIGFwcFxyXG4qL1xyXG5leHBvcnQgY2xhc3MgQXBwTW9kdWxlIHsgfSJdfQ==