"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var models_1 = require("./models");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var FootballService = /** @class */ (function () {
    function FootballService(http) {
        this.http = http;
        this.apiKey = 'cba58e88198c44c78548e2be6c21b671';
        this.baseUrl = 'https://api.football-data.org/v1';
        this.header = this.prepareHeader();
    }
    /**
     * Prepares a header with the API KEY
     */
    FootballService.prototype.prepareHeader = function () {
        var headers = new http_1.HttpHeaders();
        headers.append('X-Auth-Token', this.apiKey);
        return headers;
    };
    /**
     * List all teams for a certain competition.
     * URL Structure: https://api.football-data.org/v1/competitions/{competitionId}/teams
     */
    FootballService.prototype.getTeams = function (competitionId) {
        var url = this.baseUrl + "/competitions/" + competitionId + "/teams";
        return this.http.get(url, { headers: this.header })
            .pipe(operators_1.map(function (teams) { return models_1.FootballFactory.teamsFromRaw(teams); }));
    };
    /**
     * Show one team.
     * URL Structure: https://api.football-data.org/v1/teams/{teamId}
     */
    FootballService.prototype.getTeam = function (teamId) {
        // 1. construct a url based on https://api.football-data.org/v1/teams/{teamId}
        // 2. call http get with the url and header
        // 3. use FootballFactory.teamFromRaw to convert the output
        var url = this.baseUrl + "/teams/" + teamId;
        return this.notImplemented('getTeam');
    };
    /**
     * Show all players for a certain team.
     * URL Structure: https://api.football-data.org/v1/teams/{teamId}/players
     */
    FootballService.prototype.getPlayers = function (teamId) {
        // 1. construct a url based on https://api.football-data.org/v1/teams/{teamId}/players
        // 2. call http get with the url and header
        // 3. use FootballFactory.playersFromRaw to convert the output
        return this.notImplemented('getPlayers');
    };
    /**
     * Show all fixtures for a certain team.
     * URL Structure: https://api.football-data.org/v1/teams/{teamId}/fixtures
     */
    FootballService.prototype.getTeamFixtures = function (teamId) {
        // 1. construct a url based on https://api.football-data.org/v1/teams/{teamId}/fixtures
        // 2. call http get with the url and header
        // 3. use FootballFactory.fixturesFromRaw to convert the output
        return this.notImplemented('getTeamFixtures');
    };
    /**
     * Show League Table / current standing.
     * URL Structure: https://api.football-data.org/v1/competitions/{competitionId}/leagueTable
     * @param matchday
     */
    FootballService.prototype.getLeagueTable = function (competitionId, matchday) {
        if (matchday === void 0) { matchday = null; }
        var url = this.baseUrl + "/competitions/" + competitionId + "/leagueTable";
        var searchParams = new http_1.HttpParams();
        if (matchday) {
            searchParams = searchParams.set('matchday', matchday.toString());
        }
        return this.http.get(url, { headers: this.header, params: searchParams })
            .pipe(operators_1.map(function (competition) { return models_1.FootballFactory.leagueTableFromRaw(competition); }));
    };
    /**
     * List all fixtures for a certain competition.
     * URL Structure: https://api.football-data.org/v1/competitions/{competitionId}/fixtures
     * @param options: FixtureSearchOptions which allows to specify either a matchday or timeframe
     */
    FootballService.prototype.getFixtures = function (competitionId, options) {
        // 1. construct a url based on https://api.football-data.org/v1/competitions/{competitionId}/fixtures
        // 2. construct searchParams from options.matchday and options.timeFrame
        // see getLeagueTable as the example
        // or use this.buildSearchParams(options) if you struggle
        // 3. call http get with the url and header
        // 4. use FootballFactory.fixturesFromRaw to convert the output
        if (options === void 0) { options = {}; }
        return this.notImplemented('getFixtures');
    };
    /**
     * Constructs an http ready set of parameters based on the provided parameters.
     * @param queryParams an object containing query parameters i.e. { matchday: 5 }
     */
    FootballService.prototype.buildSearchParams = function (queryParams) {
        var searchParams = new http_1.HttpParams();
        for (var key in queryParams) {
            searchParams = searchParams.set(key, queryParams[key]);
        }
        return searchParams;
    };
    FootballService.prototype.notImplemented = function (fname) {
        return rxjs_1.throwError(fname + " Not Implemented");
    };
    FootballService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], FootballService);
    return FootballService;
}());
exports.FootballService = FootballService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9vdGJhbGwuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImZvb3RiYWxsLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkM7QUFDM0MsNkNBQTRFO0FBQzVFLG1DQUE0SDtBQUM1SCw2QkFBOEM7QUFDOUMsNENBQXFDO0FBS3JDO0lBS0UseUJBQW9CLElBQWdCO1FBQWhCLFNBQUksR0FBSixJQUFJLENBQVk7UUFKNUIsV0FBTSxHQUFHLGtDQUFrQyxDQUFDO1FBQzVDLFlBQU8sR0FBRyxrQ0FBa0MsQ0FBQztRQUluRCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUNyQyxDQUFDO0lBRUQ7O09BRUc7SUFDSyx1Q0FBYSxHQUFyQjtRQUNFLElBQU0sT0FBTyxHQUFHLElBQUksa0JBQVcsRUFBRSxDQUFDO1FBQ2xDLE9BQU8sQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUU1QyxPQUFPLE9BQU8sQ0FBQztJQUNqQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksa0NBQVEsR0FBZixVQUFnQixhQUFxQjtRQUNuQyxJQUFNLEdBQUcsR0FBTSxJQUFJLENBQUMsT0FBTyxzQkFBaUIsYUFBYSxXQUFRLENBQUM7UUFFbEUsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO2FBQ2xELElBQUksQ0FDSCxlQUFHLENBQUMsVUFBQyxLQUFVLElBQUssT0FBQSx3QkFBZSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsRUFBbkMsQ0FBbUMsQ0FBQyxDQUN6RCxDQUFDO0lBQ0osQ0FBQztJQUVEOzs7T0FHRztJQUNJLGlDQUFPLEdBQWQsVUFBZSxNQUFjO1FBQzNCLDhFQUE4RTtRQUM5RSwyQ0FBMkM7UUFDM0MsMkRBQTJEO1FBRTNELElBQU0sR0FBRyxHQUFNLElBQUksQ0FBQyxPQUFPLGVBQVUsTUFBUSxDQUFDO1FBRTlDLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUN4QyxDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksb0NBQVUsR0FBakIsVUFBa0IsTUFBYztRQUM5QixzRkFBc0Y7UUFDdEYsMkNBQTJDO1FBQzNDLDhEQUE4RDtRQUU5RCxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUVEOzs7T0FHRztJQUNJLHlDQUFlLEdBQXRCLFVBQXVCLE1BQWM7UUFDbkMsdUZBQXVGO1FBQ3ZGLDJDQUEyQztRQUMzQywrREFBK0Q7UUFFL0QsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUVEOzs7O09BSUc7SUFDSSx3Q0FBYyxHQUFyQixVQUFzQixhQUFxQixFQUFFLFFBQXVCO1FBQXZCLHlCQUFBLEVBQUEsZUFBdUI7UUFDbEUsSUFBTSxHQUFHLEdBQU0sSUFBSSxDQUFDLE9BQU8sc0JBQWlCLGFBQWEsaUJBQWMsQ0FBQztRQUV4RSxJQUFJLFlBQVksR0FBRyxJQUFJLGlCQUFVLEVBQUUsQ0FBQztRQUNwQyxJQUFJLFFBQVEsRUFBRTtZQUNaLFlBQVksR0FBRyxZQUFZLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztTQUNsRTtRQUVELE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxDQUFDO2FBQ3hFLElBQUksQ0FDSCxlQUFHLENBQUMsVUFBQSxXQUFXLElBQUksT0FBQSx3QkFBZSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsQ0FBQyxFQUEvQyxDQUErQyxDQUFDLENBQ3BFLENBQUM7SUFDSixDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLHFDQUFXLEdBQWxCLFVBQW1CLGFBQXFCLEVBQUUsT0FBa0M7UUFDMUUscUdBQXFHO1FBQ3JHLHdFQUF3RTtRQUN0RSxvQ0FBb0M7UUFDcEMseURBQXlEO1FBQzNELDJDQUEyQztRQUMzQywrREFBK0Q7UUFOdkIsd0JBQUEsRUFBQSxZQUFrQztRQVExRSxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUVEOzs7T0FHRztJQUNLLDJDQUFpQixHQUF6QixVQUEwQixXQUFnQjtRQUN4QyxJQUFJLFlBQVksR0FBRyxJQUFJLGlCQUFVLEVBQUUsQ0FBQztRQUVwQyxLQUFLLElBQUksR0FBRyxJQUFJLFdBQVcsRUFBRTtZQUMzQixZQUFZLEdBQUcsWUFBWSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7U0FDeEQ7UUFFRCxPQUFPLFlBQVksQ0FBQztJQUN0QixDQUFDO0lBRU8sd0NBQWMsR0FBdEIsVUFBdUIsS0FBYTtRQUNsQyxPQUFPLGlCQUFVLENBQUksS0FBSyxxQkFBa0IsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUF6SFUsZUFBZTtRQUgzQixpQkFBVSxDQUFDO1lBQ1YsVUFBVSxFQUFFLE1BQU07U0FDbkIsQ0FBQzt5Q0FNMEIsaUJBQVU7T0FMekIsZUFBZSxDQTBIM0I7SUFBRCxzQkFBQztDQUFBLEFBMUhELElBMEhDO0FBMUhZLDBDQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50LCAgSHR0cFBhcmFtcywgSHR0cEhlYWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IEZvb3RiYWxsRmFjdG9yeSwgQ29tcGV0aXRpb24sIExlYWd1ZVRhYmxlLCBTdGFuZGluZywgVGVhbSwgUGxheWVyLCBGaXh0dXJlLCBGaXh0dXJlU2VhcmNoT3B0aW9ucyB9IGZyb20gJy4vbW9kZWxzJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgdGhyb3dFcnJvciB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb290YmFsbFNlcnZpY2Uge1xyXG4gIHByaXZhdGUgYXBpS2V5ID0gJ2NiYTU4ZTg4MTk4YzQ0Yzc4NTQ4ZTJiZTZjMjFiNjcxJztcclxuICBwcml2YXRlIGJhc2VVcmwgPSAnaHR0cHM6Ly9hcGkuZm9vdGJhbGwtZGF0YS5vcmcvdjEnO1xyXG4gIHByaXZhdGUgaGVhZGVyOiBIdHRwSGVhZGVycztcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50KSB7XHJcbiAgICB0aGlzLmhlYWRlciA9IHRoaXMucHJlcGFyZUhlYWRlcigpO1xyXG4gIH1cclxuXHJcbiAgLyoqIFxyXG4gICAqIFByZXBhcmVzIGEgaGVhZGVyIHdpdGggdGhlIEFQSSBLRVlcclxuICAgKi9cclxuICBwcml2YXRlIHByZXBhcmVIZWFkZXIoKTogSHR0cEhlYWRlcnMge1xyXG4gICAgY29uc3QgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycygpO1xyXG4gICAgaGVhZGVycy5hcHBlbmQoJ1gtQXV0aC1Ub2tlbicsIHRoaXMuYXBpS2V5KTtcclxuXHJcbiAgICByZXR1cm4gaGVhZGVycztcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIExpc3QgYWxsIHRlYW1zIGZvciBhIGNlcnRhaW4gY29tcGV0aXRpb24uXHJcbiAgICogVVJMIFN0cnVjdHVyZTogaHR0cHM6Ly9hcGkuZm9vdGJhbGwtZGF0YS5vcmcvdjEvY29tcGV0aXRpb25zL3tjb21wZXRpdGlvbklkfS90ZWFtc1xyXG4gICAqL1xyXG4gIHB1YmxpYyBnZXRUZWFtcyhjb21wZXRpdGlvbklkOiBudW1iZXIpOiBPYnNlcnZhYmxlPFRlYW1bXT4ge1xyXG4gICAgY29uc3QgdXJsID0gYCR7dGhpcy5iYXNlVXJsfS9jb21wZXRpdGlvbnMvJHtjb21wZXRpdGlvbklkfS90ZWFtc2A7XHJcbiAgICBcclxuICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0KHVybCwgeyBoZWFkZXJzOiB0aGlzLmhlYWRlciB9KVxyXG4gICAgLnBpcGUoXHJcbiAgICAgIG1hcCgodGVhbXM6IGFueSkgPT4gRm9vdGJhbGxGYWN0b3J5LnRlYW1zRnJvbVJhdyh0ZWFtcykpXHJcbiAgICApO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogU2hvdyBvbmUgdGVhbS5cclxuICAgKiBVUkwgU3RydWN0dXJlOiBodHRwczovL2FwaS5mb290YmFsbC1kYXRhLm9yZy92MS90ZWFtcy97dGVhbUlkfVxyXG4gICAqL1xyXG4gIHB1YmxpYyBnZXRUZWFtKHRlYW1JZDogbnVtYmVyKTogT2JzZXJ2YWJsZTxUZWFtPiB7XHJcbiAgICAvLyAxLiBjb25zdHJ1Y3QgYSB1cmwgYmFzZWQgb24gaHR0cHM6Ly9hcGkuZm9vdGJhbGwtZGF0YS5vcmcvdjEvdGVhbXMve3RlYW1JZH1cclxuICAgIC8vIDIuIGNhbGwgaHR0cCBnZXQgd2l0aCB0aGUgdXJsIGFuZCBoZWFkZXJcclxuICAgIC8vIDMuIHVzZSBGb290YmFsbEZhY3RvcnkudGVhbUZyb21SYXcgdG8gY29udmVydCB0aGUgb3V0cHV0XHJcblxyXG4gICAgY29uc3QgdXJsID0gYCR7dGhpcy5iYXNlVXJsfS90ZWFtcy8ke3RlYW1JZH1gO1xyXG5cclxuICAgIHJldHVybiB0aGlzLm5vdEltcGxlbWVudGVkKCdnZXRUZWFtJyk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBTaG93IGFsbCBwbGF5ZXJzIGZvciBhIGNlcnRhaW4gdGVhbS5cclxuICAgKiBVUkwgU3RydWN0dXJlOiBodHRwczovL2FwaS5mb290YmFsbC1kYXRhLm9yZy92MS90ZWFtcy97dGVhbUlkfS9wbGF5ZXJzXHJcbiAgICovXHJcbiAgcHVibGljIGdldFBsYXllcnModGVhbUlkOiBudW1iZXIpOiBPYnNlcnZhYmxlPFBsYXllcltdPiB7XHJcbiAgICAvLyAxLiBjb25zdHJ1Y3QgYSB1cmwgYmFzZWQgb24gaHR0cHM6Ly9hcGkuZm9vdGJhbGwtZGF0YS5vcmcvdjEvdGVhbXMve3RlYW1JZH0vcGxheWVyc1xyXG4gICAgLy8gMi4gY2FsbCBodHRwIGdldCB3aXRoIHRoZSB1cmwgYW5kIGhlYWRlclxyXG4gICAgLy8gMy4gdXNlIEZvb3RiYWxsRmFjdG9yeS5wbGF5ZXJzRnJvbVJhdyB0byBjb252ZXJ0IHRoZSBvdXRwdXRcclxuICAgIFxyXG4gICAgcmV0dXJuIHRoaXMubm90SW1wbGVtZW50ZWQoJ2dldFBsYXllcnMnKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNob3cgYWxsIGZpeHR1cmVzIGZvciBhIGNlcnRhaW4gdGVhbS5cclxuICAgKiBVUkwgU3RydWN0dXJlOiBodHRwczovL2FwaS5mb290YmFsbC1kYXRhLm9yZy92MS90ZWFtcy97dGVhbUlkfS9maXh0dXJlc1xyXG4gICAqL1xyXG4gIHB1YmxpYyBnZXRUZWFtRml4dHVyZXModGVhbUlkOiBudW1iZXIpOiBPYnNlcnZhYmxlPEZpeHR1cmVbXT4ge1xyXG4gICAgLy8gMS4gY29uc3RydWN0IGEgdXJsIGJhc2VkIG9uIGh0dHBzOi8vYXBpLmZvb3RiYWxsLWRhdGEub3JnL3YxL3RlYW1zL3t0ZWFtSWR9L2ZpeHR1cmVzXHJcbiAgICAvLyAyLiBjYWxsIGh0dHAgZ2V0IHdpdGggdGhlIHVybCBhbmQgaGVhZGVyXHJcbiAgICAvLyAzLiB1c2UgRm9vdGJhbGxGYWN0b3J5LmZpeHR1cmVzRnJvbVJhdyB0byBjb252ZXJ0IHRoZSBvdXRwdXRcclxuXHJcbiAgICByZXR1cm4gdGhpcy5ub3RJbXBsZW1lbnRlZCgnZ2V0VGVhbUZpeHR1cmVzJyk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBTaG93IExlYWd1ZSBUYWJsZSAvIGN1cnJlbnQgc3RhbmRpbmcuXHRcclxuICAgKiBVUkwgU3RydWN0dXJlOiBodHRwczovL2FwaS5mb290YmFsbC1kYXRhLm9yZy92MS9jb21wZXRpdGlvbnMve2NvbXBldGl0aW9uSWR9L2xlYWd1ZVRhYmxlXHJcbiAgICogQHBhcmFtIG1hdGNoZGF5IFxyXG4gICAqL1xyXG4gIHB1YmxpYyBnZXRMZWFndWVUYWJsZShjb21wZXRpdGlvbklkOiBudW1iZXIsIG1hdGNoZGF5OiBudW1iZXIgPSBudWxsKTogT2JzZXJ2YWJsZTxMZWFndWVUYWJsZT4ge1xyXG4gICAgY29uc3QgdXJsID0gYCR7dGhpcy5iYXNlVXJsfS9jb21wZXRpdGlvbnMvJHtjb21wZXRpdGlvbklkfS9sZWFndWVUYWJsZWA7XHJcblxyXG4gICAgbGV0IHNlYXJjaFBhcmFtcyA9IG5ldyBIdHRwUGFyYW1zKCk7XHJcbiAgICBpZiAobWF0Y2hkYXkpIHtcclxuICAgICAgc2VhcmNoUGFyYW1zID0gc2VhcmNoUGFyYW1zLnNldCgnbWF0Y2hkYXknLCBtYXRjaGRheS50b1N0cmluZygpKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gdGhpcy5odHRwLmdldCh1cmwsIHsgaGVhZGVyczogdGhpcy5oZWFkZXIsIHBhcmFtczogc2VhcmNoUGFyYW1zIH0pXHJcbiAgICAucGlwZShcclxuICAgICAgbWFwKGNvbXBldGl0aW9uID0+IEZvb3RiYWxsRmFjdG9yeS5sZWFndWVUYWJsZUZyb21SYXcoY29tcGV0aXRpb24pKVxyXG4gICAgKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIExpc3QgYWxsIGZpeHR1cmVzIGZvciBhIGNlcnRhaW4gY29tcGV0aXRpb24uXHRcclxuICAgKiBVUkwgU3RydWN0dXJlOiBodHRwczovL2FwaS5mb290YmFsbC1kYXRhLm9yZy92MS9jb21wZXRpdGlvbnMve2NvbXBldGl0aW9uSWR9L2ZpeHR1cmVzXHJcbiAgICogQHBhcmFtIG9wdGlvbnM6IEZpeHR1cmVTZWFyY2hPcHRpb25zIHdoaWNoIGFsbG93cyB0byBzcGVjaWZ5IGVpdGhlciBhIG1hdGNoZGF5IG9yIHRpbWVmcmFtZVxyXG4gICAqL1xyXG4gIHB1YmxpYyBnZXRGaXh0dXJlcyhjb21wZXRpdGlvbklkOiBudW1iZXIsIG9wdGlvbnM6IEZpeHR1cmVTZWFyY2hPcHRpb25zID0ge30pOiBPYnNlcnZhYmxlPEZpeHR1cmVbXT4ge1xyXG4gICAgLy8gMS4gY29uc3RydWN0IGEgdXJsIGJhc2VkIG9uIGh0dHBzOi8vYXBpLmZvb3RiYWxsLWRhdGEub3JnL3YxL2NvbXBldGl0aW9ucy97Y29tcGV0aXRpb25JZH0vZml4dHVyZXNcclxuICAgIC8vIDIuIGNvbnN0cnVjdCBzZWFyY2hQYXJhbXMgZnJvbSBvcHRpb25zLm1hdGNoZGF5IGFuZCBvcHRpb25zLnRpbWVGcmFtZVxyXG4gICAgICAvLyBzZWUgZ2V0TGVhZ3VlVGFibGUgYXMgdGhlIGV4YW1wbGVcclxuICAgICAgLy8gb3IgdXNlIHRoaXMuYnVpbGRTZWFyY2hQYXJhbXMob3B0aW9ucykgaWYgeW91IHN0cnVnZ2xlXHJcbiAgICAvLyAzLiBjYWxsIGh0dHAgZ2V0IHdpdGggdGhlIHVybCBhbmQgaGVhZGVyXHJcbiAgICAvLyA0LiB1c2UgRm9vdGJhbGxGYWN0b3J5LmZpeHR1cmVzRnJvbVJhdyB0byBjb252ZXJ0IHRoZSBvdXRwdXRcclxuXHJcbiAgICByZXR1cm4gdGhpcy5ub3RJbXBsZW1lbnRlZCgnZ2V0Rml4dHVyZXMnKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIENvbnN0cnVjdHMgYW4gaHR0cCByZWFkeSBzZXQgb2YgcGFyYW1ldGVycyBiYXNlZCBvbiB0aGUgcHJvdmlkZWQgcGFyYW1ldGVycy5cclxuICAgKiBAcGFyYW0gcXVlcnlQYXJhbXMgYW4gb2JqZWN0IGNvbnRhaW5pbmcgcXVlcnkgcGFyYW1ldGVycyBpLmUuIHsgbWF0Y2hkYXk6IDUgfVxyXG4gICAqL1xyXG4gIHByaXZhdGUgYnVpbGRTZWFyY2hQYXJhbXMocXVlcnlQYXJhbXM6IGFueSk6IEh0dHBQYXJhbXMge1xyXG4gICAgbGV0IHNlYXJjaFBhcmFtcyA9IG5ldyBIdHRwUGFyYW1zKCk7XHJcblxyXG4gICAgZm9yIChsZXQga2V5IGluIHF1ZXJ5UGFyYW1zKSB7XHJcbiAgICAgIHNlYXJjaFBhcmFtcyA9IHNlYXJjaFBhcmFtcy5zZXQoa2V5LCBxdWVyeVBhcmFtc1trZXldKTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgcmV0dXJuIHNlYXJjaFBhcmFtcztcclxuICB9XHJcblxyXG4gIHByaXZhdGUgbm90SW1wbGVtZW50ZWQoZm5hbWU6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gdGhyb3dFcnJvcihgJHtmbmFtZX0gTm90IEltcGxlbWVudGVkYCk7XHJcbiAgfVxyXG59XHJcbiJdfQ==