"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var service_test_component_1 = require("./service-test/service-test.component");
// Lesson 1
var profile_component_1 = require("./profile/profile.component");
// Lesson 2
var color_1 = require("./color");
// Lesson 3
var tables_component_1 = require("./football/tables.component");
var competition_fixtures_component_1 = require("./football/competition-fixtures.component");
var team_component_1 = require("./football/team.component");
// Lesson 4
var wizard_profile_component_1 = require("./plugins/wizard-profile.component");
//Login
var Login_component_1 = require("./Login/Login.component");
//Login
var Caja_component_1 = require("./Caja/Caja.component");
//Productos
var Productos_component_1 = require("./Productos/Productos.component");
//Inventario
var Inventario_component_1 = require("./Inventario/Inventario.component");
//Clientes
var Clientes_component_1 = require("./Clientes/Clientes.component");
//Reportes
var Reportes_component_1 = require("./Reportes/Reportes.component");
var routes = [
    // { path: '', redirectTo: '/profile', pathMatch: 'full' },
    // { path: '', redirectTo: '/color', pathMatch: 'full' },
    // { path: '', redirectTo: '/service-test', pathMatch: 'full' },
    // { path: '', redirectTo: '/football', pathMatch: 'full' },
    // { path: '', redirectTo: '/plugins', pathMatch: 'full' },
    // Lesson 1
    { path: 'profile', component: profile_component_1.ProfileComponent },
    // Lesson 2
    { path: 'color', children: [
            { path: '', component: color_1.ColorComponent },
            { path: 'blue', component: color_1.BlueComponent },
            //.. add red and rgb routes here
            { path: 'red', component: color_1.RedComponent },
            { path: 'rgb/:rgb', component: color_1.RGBComponent },
        ] },
    // Lesson 3
    { path: 'service-test', component: service_test_component_1.ServiceTestComponent },
    { path: 'football', children: [
            { path: '', component: tables_component_1.TablesComponent },
            { path: 'fixtures/:competitionId/:competitionName', component: competition_fixtures_component_1.CompetitionFixturesComponent },
            { path: 'team/:teamId', component: team_component_1.TeamComponent }
        ] },
    // Lesson 4
    { path: 'plugins', component: wizard_profile_component_1.WizardProfileComponent },
    //Login
    { path: "", component: Login_component_1.LoginComponent },
    //Caja
    { path: "Caja", component: Caja_component_1.CajaComponent },
    //Productos
    { path: "Productos", component: Productos_component_1.ProductosComponent },
    //Inventario
    { path: "Inventario", component: Inventario_component_1.InventarioComponent },
    //Clientes
    { path: "Clientes", component: Clientes_component_1.ClientesComponent },
    //Reportes
    { path: "Reportes", component: Reportes_component_1.ReportesComponent }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.NativeScriptRouterModule.forRoot(routes)],
            exports: [router_1.NativeScriptRouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLXJvdXRpbmcubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXBwLXJvdXRpbmcubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXlDO0FBQ3pDLHNEQUF1RTtBQUd2RSxnRkFBNkU7QUFFN0UsV0FBVztBQUNYLGlFQUErRDtBQUUvRCxXQUFXO0FBQ1gsaUNBQW1GO0FBR25GLFdBQVc7QUFDWCxnRUFBOEQ7QUFDOUQsNEZBQXlGO0FBRXpGLDREQUEwRDtBQUUxRCxXQUFXO0FBQ1gsK0VBQTRFO0FBRTVFLE9BQU87QUFDUCwyREFBeUQ7QUFFekQsT0FBTztBQUNQLHdEQUFzRDtBQUV0RCxXQUFXO0FBQ1gsdUVBQXFFO0FBRXJFLFlBQVk7QUFDWiwwRUFBd0U7QUFFeEUsVUFBVTtBQUNWLG9FQUFrRTtBQUVsRSxVQUFVO0FBQ1Ysb0VBQWtFO0FBRWxFLElBQU0sTUFBTSxHQUFXO0lBQ3JCLDJEQUEyRDtJQUMzRCx5REFBeUQ7SUFDekQsZ0VBQWdFO0lBQ2hFLDREQUE0RDtJQUM1RCwyREFBMkQ7SUFFekQsV0FBVztJQUNiLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsb0NBQWdCLEVBQUU7SUFFaEQsV0FBVztJQUNYLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUU7WUFDekIsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxzQkFBYyxFQUFFO1lBQ3ZDLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUscUJBQWEsRUFBRTtZQUMxQyxnQ0FBZ0M7WUFDOUIsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxvQkFBWSxFQUFFO1lBQ3hDLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsb0JBQVksRUFBRTtTQUVoRCxFQUFDO0lBRUYsV0FBVztJQUNYLEVBQUUsSUFBSSxFQUFFLGNBQWMsRUFBRSxTQUFTLEVBQUUsNkNBQW9CLEVBQUU7SUFDekQsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRTtZQUM1QixFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLGtDQUFlLEVBQUU7WUFDeEMsRUFBRSxJQUFJLEVBQUUsMENBQTBDLEVBQUUsU0FBUyxFQUFFLDZEQUE0QixFQUFFO1lBQzdGLEVBQUUsSUFBSSxFQUFFLGNBQWMsRUFBRSxTQUFTLEVBQUUsOEJBQWEsRUFBRTtTQUNuRCxFQUFDO0lBRUYsV0FBVztJQUNYLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsaURBQXNCLEVBQUU7SUFFdEQsT0FBTztJQUNQLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsZ0NBQWMsRUFBRTtJQUV2QyxNQUFNO0lBQ04sRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSw4QkFBYSxFQUFFO0lBRTFDLFdBQVc7SUFDWCxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFFLHdDQUFrQixFQUFFO0lBRXBELFlBQVk7SUFDWixFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUFFLDBDQUFtQixFQUFFO0lBRXRELFVBQVU7SUFDVixFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLHNDQUFpQixFQUFFO0lBRWpELFVBQVU7SUFDVixFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLHNDQUFpQixFQUFFO0NBQ3BELENBQUM7QUFNRjtJQUFBO0lBQWdDLENBQUM7SUFBcEIsZ0JBQWdCO1FBSjVCLGVBQVEsQ0FBQztZQUNSLE9BQU8sRUFBRSxDQUFDLGlDQUF3QixDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNuRCxPQUFPLEVBQUUsQ0FBQyxpQ0FBd0IsQ0FBQztTQUNwQyxDQUFDO09BQ1csZ0JBQWdCLENBQUk7SUFBRCx1QkFBQztDQUFBLEFBQWpDLElBQWlDO0FBQXBCLDRDQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZSB9IGZyb20gJ25hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IFJvdXRlcyB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcblxyXG5pbXBvcnQgeyBTZXJ2aWNlVGVzdENvbXBvbmVudCB9IGZyb20gJy4vc2VydmljZS10ZXN0L3NlcnZpY2UtdGVzdC5jb21wb25lbnQnO1xyXG5cclxuLy8gTGVzc29uIDFcclxuaW1wb3J0IHsgUHJvZmlsZUNvbXBvbmVudCB9IGZyb20gJy4vcHJvZmlsZS9wcm9maWxlLmNvbXBvbmVudCc7XHJcblxyXG4vLyBMZXNzb24gMlxyXG5pbXBvcnQgeyBDb2xvckNvbXBvbmVudCwgQmx1ZUNvbXBvbmVudCwgUmVkQ29tcG9uZW50LCBSR0JDb21wb25lbnQgfSBmcm9tICcuL2NvbG9yJ1xyXG5cclxuXHJcbi8vIExlc3NvbiAzXHJcbmltcG9ydCB7IFRhYmxlc0NvbXBvbmVudCB9IGZyb20gJy4vZm9vdGJhbGwvdGFibGVzLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IENvbXBldGl0aW9uRml4dHVyZXNDb21wb25lbnQgfSBmcm9tICcuL2Zvb3RiYWxsL2NvbXBldGl0aW9uLWZpeHR1cmVzLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IExlYWd1ZVRhYmxlQ29tcG9uZW50IH0gZnJvbSAnLi9mb290YmFsbC9sZWFndWUtdGFibGUuY29tcG9uZW50JztcclxuaW1wb3J0IHsgVGVhbUNvbXBvbmVudCB9IGZyb20gJy4vZm9vdGJhbGwvdGVhbS5jb21wb25lbnQnO1xyXG5cclxuLy8gTGVzc29uIDRcclxuaW1wb3J0IHsgV2l6YXJkUHJvZmlsZUNvbXBvbmVudCB9IGZyb20gJy4vcGx1Z2lucy93aXphcmQtcHJvZmlsZS5jb21wb25lbnQnO1xyXG5cclxuLy9Mb2dpblxyXG5pbXBvcnQgeyBMb2dpbkNvbXBvbmVudCB9IGZyb20gXCIuL0xvZ2luL0xvZ2luLmNvbXBvbmVudFwiO1xyXG5cclxuLy9Mb2dpblxyXG5pbXBvcnQgeyBDYWphQ29tcG9uZW50IH0gZnJvbSBcIi4vQ2FqYS9DYWphLmNvbXBvbmVudFwiO1xyXG5cclxuLy9Qcm9kdWN0b3NcclxuaW1wb3J0IHsgUHJvZHVjdG9zQ29tcG9uZW50IH0gZnJvbSBcIi4vUHJvZHVjdG9zL1Byb2R1Y3Rvcy5jb21wb25lbnRcIjtcclxuXHJcbi8vSW52ZW50YXJpb1xyXG5pbXBvcnQgeyBJbnZlbnRhcmlvQ29tcG9uZW50IH0gZnJvbSBcIi4vSW52ZW50YXJpby9JbnZlbnRhcmlvLmNvbXBvbmVudFwiO1xyXG5cclxuLy9DbGllbnRlc1xyXG5pbXBvcnQgeyBDbGllbnRlc0NvbXBvbmVudCB9IGZyb20gXCIuL0NsaWVudGVzL0NsaWVudGVzLmNvbXBvbmVudFwiO1xyXG5cclxuLy9SZXBvcnRlc1xyXG5pbXBvcnQgeyBSZXBvcnRlc0NvbXBvbmVudCB9IGZyb20gXCIuL1JlcG9ydGVzL1JlcG9ydGVzLmNvbXBvbmVudFwiO1xyXG5cclxuY29uc3Qgcm91dGVzOiBSb3V0ZXMgPSBbXHJcbiAgLy8geyBwYXRoOiAnJywgcmVkaXJlY3RUbzogJy9wcm9maWxlJywgcGF0aE1hdGNoOiAnZnVsbCcgfSxcclxuICAvLyB7IHBhdGg6ICcnLCByZWRpcmVjdFRvOiAnL2NvbG9yJywgcGF0aE1hdGNoOiAnZnVsbCcgfSxcclxuICAvLyB7IHBhdGg6ICcnLCByZWRpcmVjdFRvOiAnL3NlcnZpY2UtdGVzdCcsIHBhdGhNYXRjaDogJ2Z1bGwnIH0sXHJcbiAgLy8geyBwYXRoOiAnJywgcmVkaXJlY3RUbzogJy9mb290YmFsbCcsIHBhdGhNYXRjaDogJ2Z1bGwnIH0sXHJcbiAgLy8geyBwYXRoOiAnJywgcmVkaXJlY3RUbzogJy9wbHVnaW5zJywgcGF0aE1hdGNoOiAnZnVsbCcgfSxcclxuXHJcbiAgICAvLyBMZXNzb24gMVxyXG4gIHsgcGF0aDogJ3Byb2ZpbGUnLCBjb21wb25lbnQ6IFByb2ZpbGVDb21wb25lbnQgfSxcclxuXHJcbiAgLy8gTGVzc29uIDJcclxuICB7IHBhdGg6ICdjb2xvcicsIGNoaWxkcmVuOiBbXHJcbiAgICB7IHBhdGg6ICcnLCBjb21wb25lbnQ6IENvbG9yQ29tcG9uZW50IH0sXHJcbiAgICB7IHBhdGg6ICdibHVlJywgY29tcG9uZW50OiBCbHVlQ29tcG9uZW50IH0sXHJcbiAgICAvLy4uIGFkZCByZWQgYW5kIHJnYiByb3V0ZXMgaGVyZVxyXG4gICAgICB7IHBhdGg6ICdyZWQnLCBjb21wb25lbnQ6IFJlZENvbXBvbmVudCB9LFxyXG4gICAgICB7IHBhdGg6ICdyZ2IvOnJnYicsIGNvbXBvbmVudDogUkdCQ29tcG9uZW50IH0sXHJcbiAgICBcclxuICBdfSxcclxuXHJcbiAgLy8gTGVzc29uIDNcclxuICB7IHBhdGg6ICdzZXJ2aWNlLXRlc3QnLCBjb21wb25lbnQ6IFNlcnZpY2VUZXN0Q29tcG9uZW50IH0sXHJcbiAgeyBwYXRoOiAnZm9vdGJhbGwnLCBjaGlsZHJlbjogW1xyXG4gICAgeyBwYXRoOiAnJywgY29tcG9uZW50OiBUYWJsZXNDb21wb25lbnQgfSxcclxuICAgIHsgcGF0aDogJ2ZpeHR1cmVzLzpjb21wZXRpdGlvbklkLzpjb21wZXRpdGlvbk5hbWUnLCBjb21wb25lbnQ6IENvbXBldGl0aW9uRml4dHVyZXNDb21wb25lbnQgfSxcclxuICAgIHsgcGF0aDogJ3RlYW0vOnRlYW1JZCcsIGNvbXBvbmVudDogVGVhbUNvbXBvbmVudCB9XHJcbiAgXX0sXHJcblxyXG4gIC8vIExlc3NvbiA0XHJcbiAgeyBwYXRoOiAncGx1Z2lucycsIGNvbXBvbmVudDogV2l6YXJkUHJvZmlsZUNvbXBvbmVudCB9LFxyXG5cclxuICAvL0xvZ2luXHJcbiAgeyBwYXRoOiBcIlwiLCBjb21wb25lbnQ6IExvZ2luQ29tcG9uZW50IH0sXHJcblxyXG4gIC8vQ2FqYVxyXG4gIHsgcGF0aDogXCJDYWphXCIsIGNvbXBvbmVudDogQ2FqYUNvbXBvbmVudCB9LFxyXG5cclxuICAvL1Byb2R1Y3Rvc1xyXG4gIHsgcGF0aDogXCJQcm9kdWN0b3NcIiwgY29tcG9uZW50OiBQcm9kdWN0b3NDb21wb25lbnQgfSxcclxuXHJcbiAgLy9JbnZlbnRhcmlvXHJcbiAgeyBwYXRoOiBcIkludmVudGFyaW9cIiwgY29tcG9uZW50OiBJbnZlbnRhcmlvQ29tcG9uZW50IH0sXHJcblxyXG4gIC8vQ2xpZW50ZXNcclxuICB7IHBhdGg6IFwiQ2xpZW50ZXNcIiwgY29tcG9uZW50OiBDbGllbnRlc0NvbXBvbmVudCB9LFxyXG5cclxuICAgLy9SZXBvcnRlc1xyXG4gICB7IHBhdGg6IFwiUmVwb3J0ZXNcIiwgY29tcG9uZW50OiBSZXBvcnRlc0NvbXBvbmVudCB9XHJcbl07XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGltcG9ydHM6IFtOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGUuZm9yUm9vdChyb3V0ZXMpXSxcclxuICBleHBvcnRzOiBbTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQXBwUm91dGluZ01vZHVsZSB7IH0iXX0=