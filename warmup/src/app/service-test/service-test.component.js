"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var football_service_1 = require("../football.service");
var rxjs_1 = require("rxjs");
var ServiceTestComponent = /** @class */ (function () {
    function ServiceTestComponent(footballService) {
        this.footballService = footballService;
    }
    ServiceTestComponent.prototype.getPLTable = function () {
        var _this = this;
        this.footballService.getLeagueTable(445)
            .subscribe(function (leagueTable) { return _this.printData(leagueTable); }, function (error) { return _this.printError(error); });
    };
    ServiceTestComponent.prototype.getPLTeams = function () {
        var _this = this;
        this.footballService.getTeams(445)
            .subscribe(function (teams) { return _this.printData(teams); }, function (error) { return _this.printError(error); });
    };
    ServiceTestComponent.prototype.getPLFixtures = function () {
        var _this = this;
        this.footballService.getFixtures(445, { timeFrame: 'p7' })
            .subscribe(function (fixtures) {
            var fixturesEssential = fixtures.map(function (fix) {
                return {
                    homeTeam: fix.homeTeamName,
                    awayTeam: fix.awayTeamName,
                    date: fix.date,
                    score: fix.result.goalsHomeTeam + ':' + fix.result.goalsAwayTeam
                };
            });
            _this.printData(fixturesEssential);
        }, function (error) { return _this.printError(error); });
    };
    ServiceTestComponent.prototype.getLiverpool = function () {
        var _this = this;
        this.footballService.getTeam(64)
            .subscribe(function (team) { return _this.printData(team); }, function (error) { return _this.printError(error); });
    };
    ServiceTestComponent.prototype.getLiverpoolPlayers = function () {
        var _this = this;
        this.footballService.getPlayers(64)
            .subscribe(function (players) { return _this.printData(players); }, function (error) { return _this.printError(error); });
    };
    ServiceTestComponent.prototype.getLiverpoolFixtures = function () {
        var _this = this;
        this.footballService.getTeamFixtures(64)
            .subscribe(function (fixtures) { return _this.printData(fixtures); }, function (error) { return _this.printError(error); });
    };
    ServiceTestComponent.prototype.printData = function (item) {
        console.log(JSON.stringify(item, null, 2));
    };
    ServiceTestComponent.prototype.printError = function (error) {
        console.log(JSON.stringify(error, null, 2));
        return rxjs_1.throwError(error);
    };
    ServiceTestComponent = __decorate([
        core_1.Component({
            selector: 'ns-test',
            moduleId: module.id,
            templateUrl: './service-test.component.html',
        }),
        __metadata("design:paramtypes", [football_service_1.FootballService])
    ], ServiceTestComponent);
    return ServiceTestComponent;
}());
exports.ServiceTestComponent = ServiceTestComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VydmljZS10ZXN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNlcnZpY2UtdGVzdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMEM7QUFFMUMsd0RBQXNEO0FBQ3RELDZCQUE4QztBQU85QztJQUVFLDhCQUFvQixlQUFnQztRQUFoQyxvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7SUFDcEQsQ0FBQztJQUVELHlDQUFVLEdBQVY7UUFBQSxpQkFNQztRQUxDLElBQUksQ0FBQyxlQUFlLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQzthQUN2QyxTQUFTLENBQ1IsVUFBQSxXQUFXLElBQUksT0FBQSxLQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxFQUEzQixDQUEyQixFQUMxQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEVBQXRCLENBQXNCLENBQ2hDLENBQUM7SUFDSixDQUFDO0lBRUQseUNBQVUsR0FBVjtRQUFBLGlCQU1DO1FBTEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDO2FBQ2pDLFNBQVMsQ0FDUixVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEVBQXJCLENBQXFCLEVBQzlCLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsRUFBdEIsQ0FBc0IsQ0FDaEMsQ0FBQztJQUNKLENBQUM7SUFFRCw0Q0FBYSxHQUFiO1FBQUEsaUJBaUJDO1FBaEJDLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsQ0FBQzthQUN6RCxTQUFTLENBQ1IsVUFBQSxRQUFRO1lBQ04sSUFBTSxpQkFBaUIsR0FBRyxRQUFRLENBQUMsR0FBRyxDQUFDLFVBQUMsR0FBWTtnQkFDbEQsT0FBTztvQkFDTCxRQUFRLEVBQUUsR0FBRyxDQUFDLFlBQVk7b0JBQzFCLFFBQVEsRUFBRSxHQUFHLENBQUMsWUFBWTtvQkFDMUIsSUFBSSxFQUFFLEdBQUcsQ0FBQyxJQUFJO29CQUNkLEtBQUssRUFBRSxHQUFHLENBQUMsTUFBTSxDQUFDLGFBQWEsR0FBRyxHQUFHLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxhQUFhO2lCQUNqRSxDQUFBO1lBQ0gsQ0FBQyxDQUFDLENBQUE7WUFFRixLQUFJLENBQUMsU0FBUyxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFDcEMsQ0FBQyxFQUNELFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsRUFBdEIsQ0FBc0IsQ0FDaEMsQ0FBQztJQUNKLENBQUM7SUFFRCwyQ0FBWSxHQUFaO1FBQUEsaUJBTUM7UUFMQyxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUM7YUFDL0IsU0FBUyxDQUNSLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBcEIsQ0FBb0IsRUFDNUIsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUF0QixDQUFzQixDQUNoQyxDQUFDO0lBQ0osQ0FBQztJQUVELGtEQUFtQixHQUFuQjtRQUFBLGlCQU1DO1FBTEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDO2FBQ2xDLFNBQVMsQ0FDUixVQUFBLE9BQU8sSUFBSSxPQUFBLEtBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEVBQXZCLENBQXVCLEVBQ2xDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsRUFBdEIsQ0FBc0IsQ0FDaEMsQ0FBQztJQUNKLENBQUM7SUFFRCxtREFBb0IsR0FBcEI7UUFBQSxpQkFNQztRQUxDLElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLEVBQUUsQ0FBQzthQUN2QyxTQUFTLENBQ1IsVUFBQSxRQUFRLElBQUksT0FBQSxLQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUF4QixDQUF3QixFQUNwQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEVBQXRCLENBQXNCLENBQ2hDLENBQUM7SUFDSixDQUFDO0lBRUQsd0NBQVMsR0FBVCxVQUFVLElBQUk7UUFDWixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzdDLENBQUM7SUFFRCx5Q0FBVSxHQUFWLFVBQVcsS0FBSztRQUNkLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDNUMsT0FBTyxpQkFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUF2RVUsb0JBQW9CO1FBTGhDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsU0FBUztZQUNuQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLCtCQUErQjtTQUMvQyxDQUFDO3lDQUdxQyxrQ0FBZTtPQUZ6QyxvQkFBb0IsQ0F3RWhDO0lBQUQsMkJBQUM7Q0FBQSxBQXhFRCxJQXdFQztBQXhFWSxvREFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRml4dHVyZSB9IGZyb20gJy4uL21vZGVscyc7XHJcbmltcG9ydCB7IEZvb3RiYWxsU2VydmljZSB9IGZyb20gJy4uL2Zvb3RiYWxsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCB0aHJvd0Vycm9yIH0gZnJvbSAncnhqcyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbnMtdGVzdCcsXHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL3NlcnZpY2UtdGVzdC5jb21wb25lbnQuaHRtbCcsXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTZXJ2aWNlVGVzdENvbXBvbmVudCB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZm9vdGJhbGxTZXJ2aWNlOiBGb290YmFsbFNlcnZpY2UpIHtcclxuICB9XHJcblxyXG4gIGdldFBMVGFibGUoKSB7XHJcbiAgICB0aGlzLmZvb3RiYWxsU2VydmljZS5nZXRMZWFndWVUYWJsZSg0NDUpXHJcbiAgICAuc3Vic2NyaWJlKFxyXG4gICAgICBsZWFndWVUYWJsZSA9PiB0aGlzLnByaW50RGF0YShsZWFndWVUYWJsZSksXHJcbiAgICAgIGVycm9yID0+IHRoaXMucHJpbnRFcnJvcihlcnJvcilcclxuICAgICk7XHJcbiAgfVxyXG5cclxuICBnZXRQTFRlYW1zKCkge1xyXG4gICAgdGhpcy5mb290YmFsbFNlcnZpY2UuZ2V0VGVhbXMoNDQ1KVxyXG4gICAgLnN1YnNjcmliZShcclxuICAgICAgdGVhbXMgPT4gdGhpcy5wcmludERhdGEodGVhbXMpLFxyXG4gICAgICBlcnJvciA9PiB0aGlzLnByaW50RXJyb3IoZXJyb3IpXHJcbiAgICApO1xyXG4gIH1cclxuXHJcbiAgZ2V0UExGaXh0dXJlcygpIHtcclxuICAgIHRoaXMuZm9vdGJhbGxTZXJ2aWNlLmdldEZpeHR1cmVzKDQ0NSwgeyB0aW1lRnJhbWU6ICdwNycgfSlcclxuICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgIGZpeHR1cmVzID0+IHtcclxuICAgICAgICBjb25zdCBmaXh0dXJlc0Vzc2VudGlhbCA9IGZpeHR1cmVzLm1hcCgoZml4OiBGaXh0dXJlKSA9PiB7XHJcbiAgICAgICAgICByZXR1cm4geyBcclxuICAgICAgICAgICAgaG9tZVRlYW06IGZpeC5ob21lVGVhbU5hbWUsXHJcbiAgICAgICAgICAgIGF3YXlUZWFtOiBmaXguYXdheVRlYW1OYW1lLFxyXG4gICAgICAgICAgICBkYXRlOiBmaXguZGF0ZSxcclxuICAgICAgICAgICAgc2NvcmU6IGZpeC5yZXN1bHQuZ29hbHNIb21lVGVhbSArICc6JyArIGZpeC5yZXN1bHQuZ29hbHNBd2F5VGVhbVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcblxyXG4gICAgICAgIHRoaXMucHJpbnREYXRhKGZpeHR1cmVzRXNzZW50aWFsKTtcclxuICAgICAgfSxcclxuICAgICAgZXJyb3IgPT4gdGhpcy5wcmludEVycm9yKGVycm9yKVxyXG4gICAgKTtcclxuICB9XHJcblxyXG4gIGdldExpdmVycG9vbCgpIHtcclxuICAgIHRoaXMuZm9vdGJhbGxTZXJ2aWNlLmdldFRlYW0oNjQpXHJcbiAgICAuc3Vic2NyaWJlKFxyXG4gICAgICB0ZWFtID0+IHRoaXMucHJpbnREYXRhKHRlYW0pLFxyXG4gICAgICBlcnJvciA9PiB0aGlzLnByaW50RXJyb3IoZXJyb3IpXHJcbiAgICApO1xyXG4gIH1cclxuXHJcbiAgZ2V0TGl2ZXJwb29sUGxheWVycygpIHtcclxuICAgIHRoaXMuZm9vdGJhbGxTZXJ2aWNlLmdldFBsYXllcnMoNjQpXHJcbiAgICAuc3Vic2NyaWJlKFxyXG4gICAgICBwbGF5ZXJzID0+IHRoaXMucHJpbnREYXRhKHBsYXllcnMpLFxyXG4gICAgICBlcnJvciA9PiB0aGlzLnByaW50RXJyb3IoZXJyb3IpXHJcbiAgICApO1xyXG4gIH1cclxuXHJcbiAgZ2V0TGl2ZXJwb29sRml4dHVyZXMoKSB7XHJcbiAgICB0aGlzLmZvb3RiYWxsU2VydmljZS5nZXRUZWFtRml4dHVyZXMoNjQpXHJcbiAgICAuc3Vic2NyaWJlKFxyXG4gICAgICBmaXh0dXJlcyA9PiB0aGlzLnByaW50RGF0YShmaXh0dXJlcyksXHJcbiAgICAgIGVycm9yID0+IHRoaXMucHJpbnRFcnJvcihlcnJvcilcclxuICAgICk7XHJcbiAgfVxyXG5cclxuICBwcmludERhdGEoaXRlbSkge1xyXG4gICAgY29uc29sZS5sb2coSlNPTi5zdHJpbmdpZnkoaXRlbSwgbnVsbCwgMikpO1xyXG4gIH1cclxuXHJcbiAgcHJpbnRFcnJvcihlcnJvcik6IE9ic2VydmFibGU8bmV2ZXI+IHtcclxuICAgIGNvbnNvbGUubG9nKEpTT04uc3RyaW5naWZ5KGVycm9yLCBudWxsLCAyKSk7XHJcbiAgICByZXR1cm4gdGhyb3dFcnJvcihlcnJvcik7XHJcbiAgfVxyXG59XHJcbiJdfQ==